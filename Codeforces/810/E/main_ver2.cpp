#include <bits/stdc++.h>

#define B 30
#define MOD 1000000007

using namespace std;

int mark[B + 1][2][2][2];
pair<int, int> dp[B + 1][2][2][2];

int apply(int &x, int y) {
	x += y;
	if (x >= MOD) {
		x -= MOD;
	}
	if (x < 0) {
		x += MOD;
	}
}

pair<int, int> calc(int b, int x, int less_x, int y, int less_y, int k, int less_k) {
	if (b < 0) {
		return make_pair(0, less_x & less_y & less_k);
	} 

	if (!mark[b][less_x][less_y][less_k]) {
		int bx = (x >> b) & 1;
		int by = (y >> b) & 1;
		int bk = (k >> b) & 1;	
		for (int vx = 0; vx < 2; vx++) {
			if (less_x || vx <= bx) {
				int nless_x = less_x | (vx < bx);
				for (int vy = 0; vy < 2; vy++) {
					if (less_y || vy <= by) {
						int nless_y = less_y | (vy < by);
						if (less_k || ((vx ^ vy) <= bk)) {
							pair<int, int> ret = calc(b - 1, x, nless_x, y, nless_y, k, less_k | ((vx ^ vy) < bk));
							apply(dp[b][less_x][less_y][less_k].first, 1LL * ret.second * ((vx ^ vy) << b) % MOD);
							apply(dp[b][less_x][less_y][less_k].first, ret.first);	
							apply(dp[b][less_x][less_y][less_k].second, ret.second);
						}
					}
				}
			}
		}
		mark[b][less_x][less_y][less_k] = 1;
	}
	return dp[b][less_x][less_y][less_k];
}

int calc(int x, int y, int k) {
	memset(dp, 0, sizeof dp);
	memset(mark, 0, sizeof mark);
	pair<int, int> ret = calc(B, x, 0, y, 0, k, 0);
	return (ret.first + ret.second) % MOD;
}

int main() {
	int T;
	scanf("%d", &T);
	while (T--) {
		int x, xx, y, yy, k;
		scanf("%d %d %d %d %d", &x, &y, &xx, &yy, &k);
		int ans = 0;
		apply(ans, calc(xx, yy, k));
		apply(ans, -calc(xx, y - 1, k));
		apply(ans, -calc(x - 1, yy, k));
		apply(ans, calc(x - 1, y - 1, k));
		printf("%d\n", ans);
	}
}