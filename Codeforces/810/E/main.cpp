#include <bits/stdc++.h>

#define K 31
#define MOD 1000000007

using namespace std;

int X1, Y1, X2, Y2, k;

inline int reduce(long long x) {
	if (x >= MOD) {
		return x - MOD;
	}
	return x;
}

int solve(int x, int y, int xx, int yy, int i, int s) {
	if (s >= k) {
		return 0;
	}
	if (X1 <= x && xx <= X2 && Y1 <= y && yy <= Y2) {
		int r = min(k - s, 1 << i);
		int f1 = (1LL * r * (r + 1) / 2) % MOD * (1LL << i) % MOD;
		int f2 = 1LL * s * r % MOD * (1LL << i) % MOD;
		return reduce(f1 + f2); 
	}
	
	if (X1 <= x && xx <= X2) {
		int r = min(k - s, 1 << i);
		int t = min(yy, Y2) - max(y, Y1) + 1;
		int f1 = (1LL * r * (r + 1) / 2) % MOD * t % MOD;
		int f2 = 1LL * s * r % MOD * t % MOD;
		return reduce(f1 + f2); 
	}

	if (Y1 <= y && yy <= Y2) {
		int r = min(k - s, 1 << i);
		int t = min(xx, X2) - max(x, X1) + 1;
		int f1 = (1LL * r * (r + 1) / 2) % MOD * t % MOD;
		int f2 = 1LL * s * r % MOD * t % MOD;
		return reduce(f1 + f2); 
	}

	int mx = (x + xx) >> 1;
	int my = (y + yy) >> 1;

	int ans = 0;
	if (mx >= X1) {
		if (my >= Y1) {
			ans = reduce(ans + solve(x, y, mx, my, i - 1, s));
		}
		if (my < Y2) {
			ans = reduce(ans + solve(x, my + 1, mx, yy, i - 1, s + (1 << (i - 1))));
		}
	}
	if (mx < X2) {
		if (my >= Y1) {
			ans = reduce(ans + solve(mx + 1, y, xx, my, i - 1, s + (1 << (i - 1))));
		}
		if (my < Y2) {
			ans = reduce(ans + solve(mx + 1, my + 1, xx, yy, i - 1, s));
		}
	}
	return ans;
}

int main() {
	int q;
	scanf("%d", &q);
	while (q--) {
		scanf("%d %d %d %d %d", &X1, &Y1, &X2, &Y2, &k);
		printf("%d\n", solve(1, 1, 1 << 30, 1 << 30, K - 1, 0));
	}
}

/*
int a[17][17];
int mark[100];

int main() {
	/*
	for (int i = 0; i < 17; i++) {
		a[i][0] = a[0][i] = i + 1;
	}
	for (int i = 1; i < 17; i++) {
		for (int j = 1; j < 17; j++) {
			memset(mark, 0, sizeof mark);
			for (int k = 0; k < j; k++) {
				mark[a[i][k]] = 1;
			}
			for (int k = 0; k < i; k++) {
				mark[a[k][j]] = 1;
			}
			for (int r = 1; r < 100; r++) {
				if (mark[r] == 0) {
					a[i][j] = r;
					break;
				}
			}
		}
	}

	for (int i = 0; i < 17; i++) {
		for (int j = 0; j < 17; j++) {
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
	
}
*/