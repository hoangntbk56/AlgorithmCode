#include <bits/stdc++.h>

#define N 300005
#define MOD 1000000007

using namespace std;

int n;
int a[N];

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", a + i);
	}
	sort(a, a + n);

	int result = 0;
	for (int i = 1, p = 1; i < n; i++) {
		p = (p + p) % MOD;
		result = (result + 1LL * (p - 1) * a[i] % MOD) % MOD;
	}
	for (int i = n - 2, p = 1; i >= 0; i--) {
		p = (p + p) % MOD;
		result = (result - 1LL * (p - 1) * a[i] % MOD + MOD) % MOD;	
	}
	printf("%d", result);
}