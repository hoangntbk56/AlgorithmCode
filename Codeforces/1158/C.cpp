#include <bits/stdc++.h>

#define N 500005

using namespace std;

int n;
int p[N], nxt[N], bit[N];
pair<int, int> a[N];

void set_bit(int i) {
    for (; i <= n; i += i & (-i)) {
        bit[i]++;
    }
}

int get_bit(int i) {
    int s = 0;
    for (; i; i -= i & (-i)) {
        s += bit[i];
    }
    return s;
}

int main() {
    int T;
    scanf("%d", &T);
    while (T--) {
        scanf("%d", &n);
        for (int i = 1; i <= n; i++) {
            scanf("%d", nxt + i);
            if (nxt[i] < 0) {
                nxt[i] = i + 1;
            }
            a[i - 1] = {-nxt[i], i};
            bit[i] = 0;
        }
        bool check = 0;
        for (int i = 1; i <= n; i++) {
            int occur = get_bit(nxt[i] - 1) - get_bit(i);
            if (occur) {
                check = true;
                printf("-1\n");
                break;
            }
            set_bit(nxt[i]);
        }
        if (!check) {
            sort(a, a + n);
            for (int i = 0, v = n; i < n; i++, v--) {
                p[a[i].second] = v;
            }
            for (int i = 1; i <= n; i++) {
                printf("%d ", p[i]);
            }
            printf("\n");
        }
    }    
    // system("pause");
}