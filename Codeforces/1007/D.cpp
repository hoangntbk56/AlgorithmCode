#include <bits/stdc++.h>

#define N 100005
#define M 10005

#define rev(u) (u ^ 1)

using namespace std;

struct SAT {
	int n;
	vector<int> st;
	vector<int> mark;
	vector<int> group;
	vector<vector<int> > edges;
	vector<vector<int> > revEdges;

	SAT(int n) {
		this->n = 2 * n;
		mark.resize(2 * n, 0);
		group.resize(2 * n, 0);
		edges.resize(2 * n);
		revEdges.resize(2 * n);
	}

	void addEdge(int u, int v) {
		edges[u].push_back(rev(v));
		edges[v].push_back(rev(u));
		revEdges[rev(v)].push_back(u);
		revEdges[rev(u)].push_back(v);
	}

	void dfs1(int u) {
		mark[u] = 1;
		for (auto &v : edges[u]) {
			if (mark[v] == 0) {
				dfs1(v);
			}
		}
		st.push_back(u);
	}

	void dfs2(int u, int k) {
		group[u] = k;
		for (auto &v : revEdges[u]) {
			if (group[v] == 0) {
				dfs2(v, k);
			}
		}
	}

	void solve() {
		for (int i = 0; i < n; i++) {
			if (mark[i] == 0) {
				dfs1(i);
			}
		}
		int k = 0;
		for (int i = st.size() - 1; i >= 0; i--) {
			int u = st[i];
			if (group[u] == 0) {
				k++;
				dfs2(u, k);
			}
		}
	}

	void answer() {
		for (int i = 0; i < n; i += 2) {
			if (group[i] == group[i | 1]) {
				printf("NO");
				return;
			}
		}
		printf("YES\n");
		for (int i = 0; i < n; i += 2) {
			printf("%d\n", (group[i] > group[i | 1]) ? 1 : 2);
		}
	}

	void print() {
		cout << "\nst\n";
		for (int x : st) {
			cout << x << " " << group[x] << "\n";
		}
		cout << "\n";
		for (int i = 0; i < n; i++) {
			cout << i << " : ";
			for (int j : edges[i]) {
				cout << j << " ";
			}
			cout << "\n";
		}
	}
} *solver;

struct LCA {
	int logn;
	vector<int> depth;
	vector<vector<int> > lca;
	vector<vector<int> > edges;

	LCA(int n, vector<int> edges[]) {
		logn = log(n) / log(2) + 1;
		depth.resize(n + 1, 0);
		lca.resize(n + 1, vector<int>(logn, 0));
		this->edges.resize(n + 1);
		for (int i = 1; i <= n; i++) {
			this->edges[i].resize(edges[i].size());
			copy(edges[i].begin(), edges[i].end(), this->edges[i].begin());
		}
		build();
	}

	void dfs(int u, int p) {
		lca[u][0] = p;
		for (int i = 1; i < logn; i++) {
			lca[u][i] = lca[lca[u][i - 1]][i - 1];
		}
		depth[u] = depth[p] + 1;
		for (auto &v : edges[u]) {
			if (p != v) {
				dfs(v, u);
			}
		}
	}

	int parent(int v, int h) {
		for (int i = logn - 1; i >= 0; i--) {
			if (h & (1 << i)) {
				v = lca[v][i];
			}
		}
		return v;
	}
	
	int get(int u, int v) {
		if (depth[u] > depth[v]) {
			swap(u, v);
		}
		int h = depth[v] - depth[u];
		v = parent(v, h);
		if (u == v) {
			return u;
		}
		for (int i = logn - 1; i >= 0; i--) {
			if (lca[u][i] != lca[v][i]) {
				u = lca[u][i];
				v = lca[v][i];
			}
		}
		return lca[u][0];
	}

	void build() {
		dfs(1, 0);
	}
} *lca;

int n, m;
vector<int> edges[N], paths[N];
vector<int> removedList[N];

set<int>* dfs(int u, int p) {
	// cout << "start " << u << "\n";
	// cout << "lca ";
	// for (auto & x : removedList[u]) {
	// 	cout << x << " ";
	// }
	// cout << "\n";
	set<int>* su = new set<int>();
	for (auto &v : edges[u]) {
		if (v != p) {
			set<int>* sv = dfs(v, u);
			if (su->size() < sv->size()) {
				swap(su, sv);
			}
			for (auto &x : *sv) {
				su->insert(x);
			}
			delete sv;
		}
	}
	for (auto &x : paths[u]) {
		su->insert(x);
	}
	for (auto &x : removedList[u]) {
		su->erase(x);
		for (auto &y : *su) {
			solver->addEdge(x, y);
		}
	}
	return su;
}

int main() {
	scanf("%d", &n);
	for (int i = 1; i < n; i++) {
		int u, v;
		scanf("%d %d", &u, &v);
		edges[u].push_back(v);
		edges[v].push_back(u);
	}
	lca = new LCA(n, edges);

	scanf("%d", &m);
	for (int i = 0; i < m; i++) {
		int a, b, c, d;
		scanf("%d %d %d %d", &a, &b, &c, &d);
		int p = lca->get(a, b);
		if (a != p) {
			paths[a].push_back(2 * i);
			removedList[lca->parent(a, lca->depth[a] - lca->depth[p] - 1)].push_back(2 * i);
		}
		a = b;
		if (a != p) {
			paths[a].push_back(2 * i);
			removedList[lca->parent(a, lca->depth[a] - lca->depth[p] - 1)].push_back(2 * i);
		}
		
		p = lca->get(c, d);
		a = c;
		if (a != p) {
			paths[a].push_back(rev(2 * i));
			removedList[lca->parent(a, lca->depth[a] - lca->depth[p] - 1)].push_back(rev(2 * i));
		}
		a = d;
		if (a != p) {
			paths[a].push_back(rev(2 * i));
			removedList[lca->parent(a, lca->depth[a] - lca->depth[p] - 1)].push_back(rev(2 * i));
		}
	}
	solver = new SAT(m);
	dfs(1, 0);
	// cout << "???\n";
	solver->solve();
	solver->answer();
	// solver->print();
}