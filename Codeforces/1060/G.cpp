#include <bits/stdc++.h> 

#define N 100005
#define K 1e9

using namespace std;

int n;
int m;
int a[N];
pair<int, int> q[N];
vector<int> group[N];

long long ans[N];

struct Node {
	int id;
	int num;
	int cnt;
	int minCnt;
	int lazyNum;
	int lazyCnt;
	int priority;
	Node *leftChild;
	Node *rightChild;

	Node(int id, int num, int cnt) {
		this->id = id;
		this->num = num;
		this->cnt = cnt;

		minCnt = cnt;
		lazyCnt = lazyNum = 0;
		leftChild = rightChild = 0;
		priority = rand();
	}
};

inline void update(Node* p) {
	p->minCnt = p->cnt;
	if (p->leftChild) {
		p->minCnt = min(p->minCnt, p->leftChild->minCnt);
	}
	if (p->rightChild) {
		p->minCnt = min(p->minCnt, p->rightChild->minCnt);
	}
}

inline void applyNum(Node* p, int f) {
	if (p) {
		p->num += f;
		p->lazyNum += f;
	}
}

inline void applyCnt(Node* p, int f) {
	if (p) {
		p->cnt -= f;
		p->minCnt -= f;
		p->lazyCnt += f;
	}
}

void lazyPropagation(Node* p) {
	if (p->lazyNum) {
		applyNum(p->leftChild, p->lazyNum);
		applyNum(p->rightChild, p->lazyNum);
		p->lazyNum = 0;
	}
	if (p->lazyCnt) {
		applyCnt(p->leftChild, p->lazyCnt);
		applyCnt(p->rightChild, p->lazyCnt);
		p->lazyCnt = 0;
	}
}

Node* merge(Node* l, Node* r) {
	if (!l || !r) {
		return l ? l : r;
	}
	if (l->priority > r->priority) {
		lazyPropagation(l);
		l->minCnt = min(l->minCnt, r->minCnt);
		l->rightChild = merge(l->rightChild, r);
		return l;
	} else {
		lazyPropagation(r);
		r->minCnt = min(l->minCnt, r->minCnt);
		r->leftChild = merge(l, r->leftChild);
		return r;
	}
}


void split(Node* p, int x, Node* &l, Node* &r) {
	// cout << "split\n";
	if (!p) {
		l = r = 0;
	} else {
		lazyPropagation(p);
		if (p->num > x) {
			r = p;
			split(p->leftChild, x, l, p->leftChild);
		} else {
			l = p;
			split(p->rightChild, x, p->rightChild, r);
		}
		update(p);
	}
}

void insert(Node* &f, Node* p) {
	if (!f) {
		f = p;
	} else {
		lazyPropagation(f);
		if (f->priority < p->priority) {
			split(f, p->num, p->leftChild, p->rightChild);
			f = p;
		} else {
			insert(f->num > p->num ? f->leftChild : f->rightChild, p);
		}
		update(f);
	}
}

Node* remove(Node* f, Node* &p, int d) {
	// cout << "remove\n";
	lazyPropagation(f);
	if (f->cnt <= d) {
		p = f;
		return merge(f->leftChild, f->rightChild);
	} else {
		if (f->leftChild && f->leftChild->minCnt == f->minCnt) {
			f->leftChild = remove(f->leftChild, p, d);
		} else {
			f->rightChild = remove(f->rightChild, p, d);
		}
		update(f);
		return f;
	}
}

void solve(Node* &f, int d, int i) {
	while (f && (i == n || f->minCnt <= d)) {
		Node* p;
		f = remove(f, p, d);
		ans[p->id] = 1LL * p->cnt * i + a[i] - (i - p->num - 1);
	}
}

int main() {
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= n; i++) {
		scanf("%d", a + i);
	}	
	for (int i = 0; i < m; i++) {
		scanf("%d %d", &q[i].first, &q[i].second);
		int j = upper_bound(a + 1, a + n + 1, q[i].first) - a - 1;
		group[j].push_back(i);
	}

	Node* root = 0;
	for (int i = 0; i <= n; i++) {
		if (i > 0 && i < n) {
			int p = (a[i + 1] - a[i] - 1) / i;
			int x = (a[i] + p * i) - (a[i + 1] - i);
			
			Node* l;
			Node* r;
			split(root, i - x - 2, l, r);
			solve(l, p + 1, i);
			solve(r, p, i);
			if (l) {
				applyNum(l, x + 1);
				applyCnt(l, p + 1);
			}
			if (r) {
				applyNum(r, x - i + 1);
				applyCnt(r, p);
			}

			root = merge(r, l);
		}

		for (int id : group[i]) {
			long long x = q[id].first;
			long long k = q[id].second;
			if (i == n || x + k * i < a[i + 1]) {
				ans[id] = x + k * i;
			} else {
				int p = (a[i + 1] - x - 1) / i;
				Node* f = new Node(id, i - a[i + 1] + x + p * i, k - p);
				insert(root, f);
			}
		}
	}
	solve(root, K, n);

	for (int i = 0; i < m; i++) {
		printf("%lld\n", ans[i]);
	}
}