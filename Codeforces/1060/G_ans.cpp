#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <assert.h>
#include <algorithm>

using namespace std;

const int maxn = 110000;

int nn;
int c[maxn];
void init_c()
{
        memset(c, 0, sizeof(c));
        for (int i = 1; i <= nn; ++i) {
                for (int j = i; j <= nn; j += (j & -j)) {
                        c[j]++;
                }
        }
}
void sub_c(int k)
{
        for (int i = k; i <= nn; i += (i & -i)) {
                c[i]--;
        }
}
int sum_c(int k)
{
        int ret = 0;
        for (int i = k; i > 0; i -= (i & -i)) {
                ret += c[i];
        }
        return ret;
}
int kth_c(int k)
{
        int x = 1, y = nn;
        while (y > x) {
                int m = x + (y - x) / 2;
                if (sum_c(m) >= k) {
                        y = m;
                }
                else {
                        x = m + 1;
                }
        }
        return y;
}

int start[maxn], len[maxn], step[maxn];
vector<int> bucket[maxn];
int cnt;
int rev_start[maxn];

int s[maxn];
void init()
{
        init_c();

        start[0] = s[nn] - nn + 1;
        len[0] = nn;
        step[0] = 0;
        cnt = 1;

        int n = nn;
        while (true) {
                int a = start[cnt - 1];
                while (n > 0 && s[n] >= a) {
                        int idx_at_cur = s[n] - a + 1;
                        int idx_at_all = kth_c(idx_at_cur);
                        bucket[cnt - 1].push_back(idx_at_all);
                        sub_c(idx_at_all);

                        n--;
                }

                if (n == 0) {
                        break;
                }

                int k = (a - s[n] + n - 1) / n;
                start[cnt] = a - n * k;
                len[cnt] = n;
                step[cnt] = step[cnt - 1] + k;

                cnt++;
        }

        for (int i = 0; i < cnt; ++i) {
                rev_start[cnt - i - 1] = start[i];
        }
}

vector<int> step1[maxn];
void gao(int m, int p, int k)
{
        int tmp = upper_bound(rev_start, rev_start + cnt, p) - rev_start;
        assert(tmp != 0);
        int i = cnt - tmp;
        int a = start[i], n = len[i];
        int dk = (p - a) / n;
        int np = p - dk * n, nk = k + dk;
        int ii = lower_bound(step, step + cnt, step[i] - nk) - step;

        step1[i].push_back(m);
        int idx_at_cur = np - a + 1;
        step1[i].push_back(idx_at_cur);
        step1[i].push_back(ii);
        step1[i].push_back(nk - (step[i] - step[ii]));
}

vector<int> step2[maxn];
long long ans[maxn];
void solve()
{
        init_c();
        for (int i = 0; i < cnt; ++i) {
                const vector<int> & tmp = step1[i];
                for (int j = 0; j < tmp.size(); j += 4) {
                        int ii = tmp[j + 2];
                        int idx_at_all = kth_c(tmp[j + 1]);
                        step2[ii].push_back(tmp[j]);
                        step2[ii].push_back(idx_at_all);
                        step2[ii].push_back(tmp[j + 3]);
                }
                const vector<int> & item = bucket[i];
                for (int j = 0; j < item.size(); ++j) {
                        sub_c(item[j]);
                }
        }

        init_c();
        for (int i = 0; i < cnt; ++i) {
                const vector<int> & tmp = step2[i];
                for (int j = 0; j < tmp.size(); j += 3) {
                        int idx_at_cur = sum_c(tmp[j + 1]);
                        long long n = len[i];
                        ans[tmp[j]] = n * tmp[j + 2] + start[i] + idx_at_cur - 1;
                }
                const vector<int> & item = bucket[i];
                for (int j = 0; j < item.size(); ++j) {
                        sub_c(item[j]);
                }
        }
}

int main()
{
        freopen("G.inp", "r", stdin);
        freopen("G.ans", "w", stdout);
        int m;
        scanf("%d %d", &nn, &m);
        for (int i = 1; i <= nn; ++i) {
                scanf("%d", &s[i]);
                s[i]++;
        }
        init();
        int p, k;
        for (int i = 1; i <= m; ++i) {
                scanf("%d %d", &p, &k);
                p++;
                if (p < s[1]) {
                        ans[i] = p;
                }
                else {
                        gao(i, p, k);
                }
        }
        solve();
        for (int i = 1; i <= m; ++i) {
                printf("%lld\n", ans[i] - 1);
        }
        return 0;
}