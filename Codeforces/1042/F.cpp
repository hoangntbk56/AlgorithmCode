#include <bits/stdc++.h>

#define N 1000005

using namespace std;

int n, k;
vector<int> edges[N];

struct Data {
	int lower;
	int upper;
	int count;

	Data() {
		lower = upper = -1;
		count = 0;
	}
} dp[N];

void dfs(int u, int f) {
	if (edges[u].size() == 1) {
		dp[u].lower = 0;
		return;
	}
	for (int v : edges[u]) {
		if (v != f) {
			dfs(v, u);
			if (dp[v].lower >= 0) {
				dp[v].lower++;
			}
			if (dp[v].upper >= 0) {
				dp[v].upper++;
			}
			if (dp[v].lower > k / 2) {
				dp[v].count += (dp[v].upper > 0);
				dp[v].upper = dp[v].lower;
				dp[v].lower = -1;
			}
			if (dp[v].upper > 0 && dp[u].lower + dp[v].upper <= k) {
				dp[u].lower = -1;
			}
			if (dp[u].upper > 0 && dp[v].lower + dp[u].upper <= k) {
				dp[v].lower = -1;
			}
			dp[u].count += dp[v].count;
			dp[u].lower = max(dp[u].lower, dp[v].lower);
			if (dp[u].upper > 0 && dp[v].upper > 0) {
				dp[u].count++;
				dp[u].upper = min(dp[u].upper, dp[v].upper);
			}
			if (dp[u].upper < 0) {
				dp[u].upper = dp[v].upper;
			}
		}
	}
}

int main() {
	scanf("%d %d", &n, &k);
	for (int i = 1, u, v; i < n; i++) {
		scanf("%d %d", &u, &v);
		edges[u].push_back(v);
		edges[v].push_back(u);
	}

	for (int u = 1; u <= n; u++) {
		if (edges[u].size() > 1) {
			dfs(u, 0);
			int res = dp[u].count + (dp[u].lower > 0) + (dp[u].upper > 0);
			printf("%d", res);
			return 0;
		}
	}
}