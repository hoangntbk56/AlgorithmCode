#include <bits/stdc++.h>

using namespace std;

const int N = 100010;
const int B = 500;

int n, m;
int t[N];
int block[B], lazy[B], state[N + N];
long long l[N], r[N], a[N + N];

void apply(int u) {
	if (block[u] != 0) {
		for (int j = u * B; j < (u + 1) * B; j++) {
			state[j] = block[u];
		}
		block[u] = 0;
	} else {
		if (lazy[u]) {
			for (int j = u * B; j < (u + 1) * B; j++) {
				state[j] = -state[j];
			}
			lazy[u] = 0;
		}	
	}
}

void assign(int x, int y, int u, int v, int val) {
	for (int j = u + 1; j < v; j++) {
		block[j] = val;
		lazy[j] = 0;
	}			
	apply(u);
	apply(v);
	if (u != v) {
		for (int j = x; j < (u + 1) * B; j++) {
			state[j] = val;
		}
		for (int j = v * B; j <= y; j++) {
			state[j] = val;
		}			
	} else {
			for (int j = x; j <= y; j++) {
			state[j] = val;		
		}
	}
}

void invert(int x, int y, int u, int v) {
	for (int j = u + 1; j < v; j++) {
		if (block[j] != 0) {
			block[j] = -block[j];
		} else {
			lazy[j] ^= 1;
		}
	}
	apply(u);
	apply(v);
	if (u != v) {
		for (int j = x; j < (u + 1) * B; j++) {
			state[j] = -state[j];
		}
		for (int j = v * B; j <= y; j++) {
			state[j] = -state[j];
		}			
	} else {
			for (int j = x; j <= y; j++) {
			state[j] = -state[j];		
		}
	}
}

void printAns() {
	for (int i = 0; i <= m / B; i++) {
		if (block[i] == -1) {
			printf("%lld\n", a[i * B]);
			return;
		} else if (block[i] == 0) {
			apply(i);
			for (int j = i * B; j < (i + 1) * B; j++) {
				if (state[j] == -1) {
					printf("%lld\n", a[j]);
					return;
				}
			}
		}
	}
}

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d %lld %lld", t + i, l + i, r + i);
		a[i + i + 1] = r[i] + 1;
		a[i + i] = l[i];
	}

	a[n + n] = 1;
	sort(a, a + n + n + 1);
	m = unique(a, a + n + n + 1) - a;
	for (int i = 0; i <= m / B; i++) {
		block[i] = -1;
	}

	for (int i = 0; i < n; i++) {
		//cout << l[i] << " " << r[i] << " ";
		int x = lower_bound(a, a + m, l[i]) - a;
		int y = lower_bound(a, a + m, r[i]) - a;
		if (a[y] > r[i]) {
			y--;
		}
		int u = x / B;
		int v = y / B;
		if (t[i] == 1) {	
			assign(x, y, u, v, 1);
		} else if (t[i] == 2) {
			assign(x, y, u, v, -1);
		} else {
			invert(x, y, u, v);
		}
		printAns();
	}
}
