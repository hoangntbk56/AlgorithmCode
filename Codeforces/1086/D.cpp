#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp> 
#include <ext/pb_ds/detail/standard_policies.hpp>

#define N 200005

using namespace std;
using namespace __gnu_pbds;

typedef tree<int,
						null_type,
						less<int>,
						rb_tree_tag,
						tree_order_statistics_node_update> ordered_set;

int n, m;
int a[N];
string st;
ordered_set tr[3];

int ctoi(char c) {
	switch (c) {
		case 'R': return 0;
		case 'P': return 1;
		case 'S': return 2;
	}
	return -999999999;
}

int getRes() {
	int res = 0;
	for (int i = 0; i < 3; i++) {
		if (tr[(i + 1) % 3].size() == 0) {
			res += tr[i].size();
		} else if (tr[(i + 2) % 3].size() > 0) {
			int x, y;
			vector<pair<int, int> > l, r;
			x = *tr[(i + 1) % 3].rbegin();
			y = *tr[(i + 2) % 3].rbegin();
			if (x < y) {
				l.push_back({1, n});
			} else {
				l.push_back({1, y - 1});
				l.push_back({x + 1, n});
			}
			x = *tr[(i + 1) % 3].begin();
			y = *tr[(i + 2) % 3].begin();
			if (x > y) {
				r.push_back({1, n});
			} else {
				r.push_back({y + 1, n});
				r.push_back({1, x - 1});
			}
			for (auto &u : l) {
				for (auto &v : r) {
					x = max(u.first, v.first);
					y = min(u.second, v.second);
					// cout << u.first << " " << u.second << " " << v.first << " " << v.second << "\n";
					if (x <= y) {
						// cout << x << " " << y << "\n";
						x = tr[i].order_of_key(x);
						y = tr[i].order_of_key(y + 1);
						res += y - x;
						// cout << i << " --- " << res << "\n";
					}
				}
			}
		}
	}
	return res;
}

int main() {
	cin >> n >> m >> st;
	for (int i = 1; i <= n; i++) {
		a[i] = ctoi(st[i - 1]);
		tr[a[i]].insert(i);
	}
	cout << getRes() << "\n";
	for (int i = 0; i < m; i++) {
		int p;
		char c;
		cin >> p >> c;
		tr[a[p]].erase(p);
		a[p] = ctoi(c);
		tr[a[p]].insert(p);
		cout << getRes() << "\n";
		// return 0;
	}
}