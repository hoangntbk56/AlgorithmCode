#include <bits/stdc++.h>

using namespace std;

struct Node {
	int val;
	int lazy;
	int priority;
	Node *left, *right;

	Node(int v) {
		val = v;
		lazy = 0;
		priority = rand();
		left = right = 0;
	}
};

void apply(Node* p, int inc) {
	if (p) {
		p->val += inc;
		p->lazy += inc;
	}
}

void propagate(Node* p) {
	if (p->lazy) {
		apply(p->left, p->lazy);
		apply(p->right, p->lazy);
		p->lazy = 0;
	}
}

void split(Node* p, Node* &l, Node* &r, int key) {
	if (!p) {
		l = r = 0;
	} else {
		propagate(p);
		if (p->val >= key) {
			split(p->left, l, p->left, key);
			r = p;
		} else {
			split(p->right, p->right, r, key);
			l = p;
		}
	}
}

void insert(Node* &p, Node* x) {
	if (!p) {
		p = x;
	} else {
		propagate(p);
		if (p->priority < x->priority) {
			split(p, x->left, x->right, x->val);
			p = x;
		} else {
			insert(p->val < x->val ? p->right : p->left, x);
		}
	}
}

Node* merge(Node* l, Node* r) {
	if (!l || !r) {
		return l ? l : r;
	}
	if (l->priority > r->priority) {
		propagate(l);
		l->right = merge(l->right, r);
		return l;
	} else {
		propagate(r);
		r->left = merge(l, r->left);
		return r;
	}
}

void del(Node* &p) {
	if (p->left) {
		del(p->left);
	} else {
		p = p->right;
	}
}

int dfs(Node* p) {
	if (p) {
		return dfs(p->left) + dfs(p->right) + 1;
	} else {
		return 0;
	}
}

void display(Node* p) {
	if (p) {
		propagate(p);
		display(p->left);
		cout << p->val << " ";
		display(p->right);
	}
}

int main() {
	int n;
	scanf("%d", &n);

	Node* root = 0;
	for (int i = 0; i < n; i++) {
		int l, r;
		scanf("%d %d", &l, &r);

		Node *tl, *tr, *tmp;
		split(root, tl, tmp, l);
		split(tmp, tmp, tr, r);

		apply(tmp, 1);
		if (tr) {
			del(tr);
		}

		root = merge(tl, tmp);
		root = merge(root, tr);
		insert(root, new Node(l));

		//display(root); cout << " .....\n";
	}

	printf("%d\n", dfs(root));
}