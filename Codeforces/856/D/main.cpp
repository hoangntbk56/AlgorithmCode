#include <bits/stdc++.h>

#define N 200010

using namespace std;

int n, m;
int dp[N], dep[N], sum[N], lca[N][20], acc[N][20];
vector<int> edges[N], capche[N][20];
vector<pair<pair<int, int>, int> > q[N];

int jump(int u, int h) {
	for (int i = 19; i >= 0; i--) {
		if (h & (1 << i)) {
			u = lca[u][i];
		}
	}
	return u;
}

int getLCA(int u, int v) {
	if (dep[u] > dep[v]) {
		swap(u, v);
	}
	v = jump(v, dep[v] - dep[u]);
	for (int i = 19; i >= 0; i--) {
		if (lca[u][i] != lca[v][i]) {
			u = lca[u][i];
			v = lca[v][i];
		}
	}
	return u != v ? lca[u][0] : u;
}

int calc(int u, int h) {
	int w = 0;
	for (int i = 19; i >= 0; i--) {
		if (h & (1 << i)) {
			w += acc[u][i];
			u = lca[u][i];
		}
	}
	return w;
}

int main() {
	scanf("%d %d", &n, &m);
	dep[1] = 1;
	for (int i = 2, p; i <= n; i++) {
		scanf("%d", &p);
		lca[i][0] = p;
		dep[i] = dep[p] + 1;
		for (int j = 1; j < 20; j++) {
			lca[i][j] = lca[lca[i][j - 1]][j - 1];
		}
		edges[p].push_back(i);
	}	

	for (int i = 0; i < m; i++) {
		int u, v, w;
		scanf("%d %d %d", &u, &v, &w);
		int p = getLCA(u, v);
		q[p].push_back(make_pair(make_pair(u, v), w));
	}

	for (int u = n; u; u--) {
		sum[u] = 0;
		for (int v : edges[u]) {
			sum[u] += dp[v];
			capche[u][0].push_back(v);
		}
		for (int v : edges[u]) {
			acc[v][0] = sum[u] - dp[v];
		}
		for (int i = 0; i < 19; i++) {
			for (int x : capche[u][i]) {
				for (int y : capche[x][i]) {
					acc[y][i + 1] = acc[x][i] + acc[y][i];
					capche[u][i + 1].push_back(y);
				}
				capche[x][i].clear();
			}
		}
		
		dp[u] = sum[u];
		for (auto e : q[u]) {
			int x = e.first.first;
			int y = e.first.second;
			int beauty = sum[u] + e.second;
			if (x != u) {
				int nx = jump(x, dep[x] - dep[u] - 1);
				beauty -= dp[nx];
				beauty += sum[x] + calc(x, dep[x] - dep[u] - 1);
			} 
			if (y != u) {
				int ny = jump(y, dep[y] - dep[u] - 1);
				beauty -= dp[ny];
				beauty += sum[y] + calc(y, dep[y] - dep[u] - 1);
			}
			dp[u] = max(dp[u], beauty);
			
		}
		
	}
	printf("%d\n", dp[1]);
}