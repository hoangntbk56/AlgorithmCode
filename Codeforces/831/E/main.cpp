#include <bits/stdc++.h>

#define N 100005

using namespace std;

int n, t[N];
set<int> pos[N];

void update(int i) {
	for (; i < N; i += i & (-i)) {
		t[i]++;
	}
}

int get(int i) {
	int w = 0;
	for (; i; i -= i & (-i)) {
		w += t[i];
	}
	return w;
}

int f(int l, int r) {
	return r - l - get(r) + get(l);
}

int main() {
	n = 100000;//scanf("%d", &n);
	for (int i = 1, x; i <= n; i++) {
		x = rand() % 5 + 1;//scanf("%d", &x);
		pos[x].insert(i);
	}

	long long ans = 0;
	for (int i = 1, cur = 0; i < N; i++) {
		while (!pos[i].empty()) {
			auto it = pos[i].upper_bound(cur);
			if (it == pos[i].end()) {
				ans += f(cur, n);
				//cout << " case 1: " << *it << " " << cur << " " << ans << "\n";
				cur = 0;
				
			} else {
				ans += f(cur, *it);
				//cout << " case 2: " << *it << " " << cur << " " << ans << "\n";
				cur = *it;
				pos[i].erase(it);
				update(cur);
				
			}
		}
	}

	printf("%lld\n", ans);
}