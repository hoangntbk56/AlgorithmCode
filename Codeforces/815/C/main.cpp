#include <bits/stdc++.h>

#define N 5005

using namespace std;

int n, limit;
int c[N], d[N], sz[N];
long long dp[N][N][2];
vector<int> children[N];

void solve(int u) {
	sz[u]++;
	dp[u][1][0] = c[u];
	dp[u][1][1] = c[u] - d[u];
	dp[u][0][1] = limit + 1;
	for (int v : children[u]) {
		solve(v);
		for (int i = sz[u]; i >= 0; i--) {
			for (int j = sz[v]; j >= 0; j--) {
				dp[u][i + j][0] = min(dp[u][i + j][0], dp[u][i][0] + dp[v][j][0]);
				dp[u][i + j][1] = min(dp[u][i + j][1], dp[u][i][1] + min(dp[v][j][0], dp[v][j][1]));
			}
		}
		sz[u] += sz[v];
	}	
}

int main() {
	scanf("%d %d", &n, &limit);
	scanf("%d %d", c + 1, d + 1);
	for (int i = 2, p; i <= n; i++) {
		scanf("%d %d %d", c + i, d + i, &p);
		children[p].push_back(i);
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			dp[i][j][0] = dp[i][j][1] = limit + 1;
		}
	}
	solve(1);
	for (int i = n; i >= 0; i--) {
		if (dp[1][i][0] <= limit || dp[1][i][1] <= limit) {
			printf("%d\n", i);
			return 0;
		}
	}
}