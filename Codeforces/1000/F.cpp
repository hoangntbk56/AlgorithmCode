#include <bits/stdc++.h>

#define N 5000010

using namespace std;

int n, m;
int a[N];
int nex[N], pre[N], pos[N];
int ans[N];
pair<int, int> tree[N << 2];

struct Segment {
	int l, r;
	int id;

	bool operator < (const Segment &other) const {
		return l < other.l;
	}
} query[N], seg[N];

void set_tree(int l, int r, int i, int pos, int upper) {
	if (l == r) {
		tree[i] = make_pair(upper, a[pos]);
	} else {
		int mid = (l + r) >> 1;
		if (mid >= pos) {
			set_tree(l, mid, i + i, pos, upper);
		} else {
			set_tree(mid + 1, r, i + i + 1, pos, upper);
		}
		tree[i] = max(tree[i + i], tree[i + i + 1]);
	}
}

pair<int, int> get_tree(int l, int r, int x, int y, int i) {
	if (x <= l && r <= y) {
		return tree[i];
	}
	if (l > y || r < x) {
		return make_pair(0, 0);
	} else {
		int mid = (l + r) >> 1;
		return max(get_tree(l, mid, x, y, i + i), get_tree(mid + 1, r, x, y, i + i + 1));
	}
}

int main() {
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%d", a + i);
	}
	for (int i = 1; i <= n; i++) {
		pre[i] = pos[a[i]];
		pos[a[i]] = i;
	}
	for (int i = 0; i < N; i++) {
		pos[i] = n + 1;
	}
	for (int i = n; i > 0; i--) {
		nex[i] = pos[a[i]];
		pos[a[i]] = i;
	}
	for (int i = 1; i <= n; i++) {
		seg[i - 1] = Segment{pre[i], nex[i], i};
	}
	sort(seg, seg + n);
	scanf("%d", &m);
	for (int i = 0; i < m; i++) {
		scanf("%d %d", &query[i].l, &query[i].r);
		query[i].id = i;
	}
	sort(query, query + m);
	for (int i = 0, j = 0; j < m; j++) {
		while (i < n && seg[i].l < query[j].l) {
			//cout << "i : " << seg[i].l << " " << seg[i].id << " " << seg[i].r << "\n";
			set_tree(1, n, 1, seg[i].id, seg[i].r);//set_tree(seg[i].id, seg[i].r);
			i++;
		}
		//cout << "j : " << query[j].l << " " << query[j].r << "\n";
		pair<int, int> ret = get_tree(1, n, query[j].l, query[j].r, 1);
		//cout << "ans\n";
		ans[query[j].id] = ret.first > query[j].r ? ret.second : 0;
	}
	for (int i = 0; i < m; i++) {
		printf("%d\n", ans[i]);
	}
} 