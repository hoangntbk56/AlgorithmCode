#include <bits/stdc++.h>

#define N 100005
#define K 300005
#define MAX 1000000005

using namespace std;

int n, m, k;
int minVal[N];

struct Segment {
	int l, r, p;

	Segment() {}
	Segment(int l, int r, int p) : l(l), r(r), p(p) {};

	bool operator < (const Segment &other) const {
		return l > other.l || (l == other.l && r < other.r);
	}
} segs[K];

struct Node {
	Node *l, *r;
	int val;

	Node(Node* l, Node* r, int v) : l(l), r(r), val(v) {}
} *root[K];

Node* initTree(int l, int r) {
	if (l == r) {
		minVal[l] = MAX;
		return new Node(0, 0, MAX);
	} else {
		int mid = (l + r) >> 1;
		return new Node(initTree(l, mid), initTree(mid + 1, r), MAX);
	}
}

Node* updateTree(int l, int r, Node* p, int x, int v) {
	if (l == r) {
		return new Node(0, 0, v);
	} else {
		int mid = (l + r) >> 1;
		if (mid >= x) {
			Node* newNode = updateTree(l, mid, p->l, x, v);
			return new Node(newNode, p->r, max(newNode->val, p->r->val));
		} else {
			Node* newNode = updateTree(mid + 1, r, p->r, x, v);
			return new Node(p->l, newNode, max(p->l->val, newNode->val));
		}
	}
}

int query(int l, int r, Node* p, int x, int y) {
	if (l > y || r < x) {
		return 0;
	}
	if (l >= x && r <= y) {
		return p->val;
	}
	int mid = (l + r) >> 1;
	return max(query(l, mid, p->l, x, y), query(mid + 1, r, p->r, x, y));
}

int main() {
	ios_base::sync_with_stdio(0);
	cin >> n >> m >> k;
	for (int i = 0; i < k; i++) {
		cin >> segs[i].l >> segs[i].r >> segs[i].p;
	}
	sort(segs, segs + k);
	root[0] = initTree(1, n);
	for (int i = 0; i < k; i++) {
		if (minVal[segs[i].p] > segs[i].r) {
			root[i + 1] = updateTree(1, n, root[i], segs[i].p, segs[i].r);
			minVal[segs[i].p] = segs[i].r;
		} else {
			root[i + 1] = root[i];
		}
	}
	for (int i = 0; i < m; i++) {
		int a, b, x, y;
		cin >> a >> b >> x >> y;
		int j = (int) (upper_bound(segs, segs + k, Segment(x, MAX, MAX)) - segs);
		int v = query(1, n, root[j], a, b);
		if (v <= y) {
			cout << "yes\n";
		} else {
			cout << "no\n";
		}
		cout.flush();
	}
}