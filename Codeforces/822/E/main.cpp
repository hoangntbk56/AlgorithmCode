#include <bits/stdc++.h>

#define N 100010
#define K 35

#define B 1993
#define P1 1000000403
#define P2 1000000409

using namespace std;

typedef pair<int, int> Hash;

Hash operator + (Hash a, Hash b) {
	return Hash((a.first + b.first) % P1, (a.second + b.second) % P2);
}

Hash operator - (Hash a, Hash b) {
	return Hash((a.first - b.first + P1) % P1, (a.second - b.second + P2) % P2);
}

Hash operator * (Hash a, Hash b) {
	return Hash(1LL * a.first * b.first % P1, 1LL * a.second * b.second % P2);
}

Hash operator + (Hash a, int b) {
	return a + Hash(b, b);
}

Hash operator * (Hash a, int b) {
	return a * Hash(b, b);
}

int n, m, k;
char a[N], b[N];

int dp[K][N];

Hash p[N], hash_a[N], hash_b[N];

void build(int n, char a[], Hash h[]) {
	h[0] = Hash(0, 0);
	for (int i = 1; i <= n; i++) {
		h[i] = h[i - 1] * B + a[i];
	}
}

Hash get(Hash h[], int l, int r) {
	return h[r] - (h[l - 1] * p[r - l + 1]);
}

int main() {
	scanf("%d\n", &n);
	scanf("%s\n", a + 1);
	scanf("%d\n", &m);
	scanf("%s\n", b + 1);
	scanf("%d", &k);

	p[0] = Hash(1, 1);
	for (int i = 1; i <= n; i++) {
		p[i] = p[i - 1] * B;
	}

	build(n, a, hash_a);
	build(m, b, hash_b);

	for (int t = 1; t <= k; t++) {
		for (int i = 1; i <= n; i++) {
			dp[t][i] = max(dp[t][i], dp[t][i - 1]);
			dp[t][i] = max(dp[t][i], dp[t - 1][i]);
			
			int l = 0;
			int r = m - dp[t - 1][i - 1] + 1;
			int j = dp[t - 1][i - 1] + 1;
			while (r - l > 1) {
				int mid = (l + r) >> 1;
				if (get(hash_a, i, i + mid - 1) == get(hash_b, j, j + mid - 1)) {
					l = mid;
				} else {
					r = mid;
				}
			}

			if (l > 0) {
				dp[t][i + l - 1] = max(dp[t][i + l - 1], dp[t - 1][i - 1] + l);
			}
		}
	}

	if (dp[k][n] == m) {
		printf("YES");
	} else {
		printf("NO");
	}
}