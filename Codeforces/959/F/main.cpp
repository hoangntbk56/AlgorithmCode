#include <bits/stdc++.h>

using namespace std;

const int N = 100005;

int n, m;
int a[N], out[N];
vector<pair<int, int> > q[N];

int main() {
	cin >> n >> m;
	for (int i = 1; i <= n; i++) {
		cin >> a[i];
	}
	for (int i = 1; i <= m; i++) {
		int l, x;
		cin >> l >> x;
		q[l].emplace_back(i, x);
	}
	set<int> s;
	s.insert(0);
	int num = 1;
	for (int i = 1; i <= n; i++) {
		if (s.count(a[i])) {
			num = 1LL * num * 2 % 1000000007;
		} else {
			vector<int> v;
			for (int u : s) {
				v.push_back(u ^ a[i]);
			}
			for (int u : v) {
				s.insert(u);
			}
		}
		// cout << s.size() << " -\n";
		for (auto &p : q[i]) {
			out[p.first] = s.count(p.second) ? num : 0;
		}
	}
	for (int i = 1; i <= m; i++) {
		cout << out[i] << "\n";
	}
}