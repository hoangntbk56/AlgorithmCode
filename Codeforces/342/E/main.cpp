#include <bits/stdc++.h>

using namespace std;

const int N = 100010;

int n, m;
int nbAns;
int ans[N];
int visited[N], len[N], cnt[N], group[N];
vector<pair<int, int> > *queries[N];
vector<int> edges[N];

int dfs(int u, int f) {
	cnt[u] = 1;
	for (auto &v : edges[u]) {
		if (v != f && !visited[v]) {
			cnt[u] += dfs(v, u);
		}
	}
	return cnt[u];
}

int findCentroid(int r, int sz) {
	int f = r;
	bool stop = 0;
	while (!stop) {
		stop = 1;
		for (auto &v : edges[r]) {
			if (v != f && !visited[v] && cnt[v] >= sz / 2) {
				f = r;
				r = v;
				stop = 0;
				break;
			}
		}
	}
	return r;
}

void build(int u, int f, int d) {
	len[u] = d;
	for (auto &v : edges[u]) {
		if (v != f && !visited[v]) {
			build(v, u, d + 1);
		}
	}
}

void mark(int u, int f, int g) {
	group[u] = g;
	for (auto &v : edges[u]) {
		if (v != f && !visited[v]) {
			mark(v, u, g);
		}
	}
}

void solve(int r) {
	int sz = dfs(r, 0);
	int centroid = findCentroid(r, sz);

	build(centroid, 0, 0);
	queries[centroid] = queries[r];

	int val = N;
	for (auto &q : *queries[centroid]) {
		if (q.first == 1) {
			val = min(val, len[q.second]);
		} else {
			ans[q.first - 2] = min(ans[q.first - 2], val + len[q.second]);
		}
	}

	for (auto &v : edges[centroid]) {
		if (!visited[v]) {
			mark(v, centroid, v);
			queries[v] = new vector<pair<int, int> >();
		}
	}

	for (auto &q : *queries[centroid]) {
		if (q.second != centroid) {
			queries[group[q.second]]->emplace_back(q);
		}
	}

	visited[centroid] = 1;
	for (auto &v : edges[centroid]) {
		if (!visited[v]) {
			solve(v);
		}
	}
}

int main() {
	scanf("%d %d", &n, &m);
	for (int i = 1; i < n; i++) {
		int u, v;
		scanf("%d %d", &u, &v);
		edges[u].push_back(v);
		edges[v].push_back(u);
	}

	queries[1] = new vector<pair<int, int> >();
	queries[1]->emplace_back(1, 1);
	for (int i = 1; i <= m; i++) {
		int t, v;
		scanf("%d %d", &t, &v);
		if (t == 1) {
			queries[1]->emplace_back(t, v);
		} else {
			queries[1]->emplace_back(t + nbAns, v);
			ans[nbAns++] = N;
		}
	}

	solve(1);
	for (int i = 0; i < nbAns; i++) {
		printf("%d\n", ans[i]);
	}
}