#include <bits/stdc++.h>

using namespace std;

const int N = 10;
const int M = 100010;

int n, m, q;
int a[N][M];
int state[50], mark[50];

struct Info{
	int cnt;
	int left[N];
	int right[N];

	Info() {
		cnt = 0;
		memset(left, 0, sizeof left);
		memset(right, 0, sizeof right);
	}
} tr[M << 2];

Info join(Info &u, Info &v, int p) {
	Info f;
	f.cnt = u.cnt + v.cnt;
	memset(state, 0, sizeof state);
	for (int k = 0; k < n; k++) {
		if (a[k][p] == a[k][p + 1]) {
			int x = u.right[k];
			int y = v.left[k] + 21;
			while (state[x]) {
				x = state[x];
			}
			while (state[y]) {
				y = state[y];
			}
			if (x != y) {
				f.cnt--;
				state[y] = x;
			}
		}
	}
	int g = 1;
	memset(mark, 0, sizeof mark);
	for (int k = 0; k < n; k++) {
		int x = u.left[k];
		while (state[x]) {
			x = state[x];
		}
		if (mark[x] == 0) {
			mark[x] = g++;
		}
		f.left[k] = mark[x];
		int y = v.right[k] + 21;
		while (state[y]) {
			y = state[y];
		}
		if (mark[y] == 0) {
			mark[y] = g++;
		}
		f.right[k] = mark[y];
	}
	return f;
}

void build(int l, int r, int i) {
	if (l == r) {
		for (int k = 0; k < n; k++) {
			if (k == 0 || a[k][l] != a[k - 1][l]) {
				tr[i].left[k] = tr[i].right[k] = ++tr[i].cnt;	
			} else {
				tr[i].left[k] = tr[i].right[k] = tr[i].left[k - 1];
			}
		}
	} else {
		int mid = (l + r) >> 1;
		build(l, mid, i + i);
		build(mid + 1, r, i + i + 1);
		tr[i] = join(tr[i + i], tr[i + i + 1], mid);
	}
	/*
	cout << " + " << l << " " << r << "\n";
	for (int k = 0; k < n; k++) {
		cout << tr[i].left[k] << " ";
	}
	cout << "\n";
	for (int k = 0; k < n; k++) {
		cout << tr[i].right[k] << " ";
	}
	cout << "\n";
	*/
}

void get(int l, int r, int x, int y, int i, Info &res) {
	if (x <= l && r <= y) {
		if (l == x) {
			res = tr[i];
		} else {
			res = join(res, tr[i], l - 1);
		}
	} else {
		int mid = (l + r) >> 1;
		if (mid >= x) {
			get(l, mid, x, y, i + i, res);
		}
		if (mid < y) {
			get(mid + 1, r, x, y, i + i + 1, res);
		}
	}
}

int main() {
	//scanf("%d %d %d", &n, &m, &q);
	freopen("out", "w", stdout);
	n = 10;
	m = 100000;
	q = 100000;
	for (int i = 0; i < n; i++) {
		for (int j = 1; j <= m; j++) {
			a[i][j] = rand() % 2 + 1;//scanf("%d", &a[i][j]);
		}
	}
	build(1, m, 1);
	Info res;
	while (q--) {
		int l, r;
		l = rand() % m + 1;
		r = rand() % m + 1;
		if (l > r) {
			swap(l, r);
		}
		//scanf("%d %d", &l, &r);
		get(1, m, l, r, 1, res);
		printf("%d\n", res.cnt);
	}
}