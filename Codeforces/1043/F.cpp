#include <bits/stdc++.h>

#define N 300001
#define H 1000000007

using namespace std;

int n;
int p[N];
int s[N];
int cnt[N];
int inv[N];
int dp[N];

int f(int x, int y) {
	int r = 1;
	while (y) {
		if (y & 1) {
			r = 1LL * r * x % H;
		}
		x = 1LL * x * x % H;
		y >>= 1;
	}
	return r;
}

int main() {
	scanf("%d", &n);
	int g = 0;
	for (int i = 0, v; i < n; i++) {
		scanf("%d", &v);
		cnt[v]++;
		if (g) {
			g = __gcd(g, v);
		} else {
			g = v;
		}
	}
	
	if (g > 1) {
		printf("-1");
	} else {
		p[0] = inv[0] = 1;
		for (int i = 1; i < N; i++) {
			p[i] = 1LL * p[i - 1] * i % H;
			inv[i] = f(p[i], H - 2);
			for (int j = i + i; j < N; j += i) {
				cnt[i] += cnt[j];
			}
			dp[i] = cnt[i];
		}
		int ans = 1;
		while (true) {
			for (int i = N - 1; i; i--) {
				dp[i] = 0;
				if (cnt[i] >= ans) {
					dp[i] = 1LL * p[cnt[i]] * inv[ans] % H * inv[cnt[i] - ans] % H;
					for (int j = i + i; j < N; j += i) {
						dp[i] -= dp[j];
						if (dp[i] < 0) {
							dp[i] += H;
						}
					}
				}
			}
			if (dp[1]) {
				printf("%d", ans);
				return 0;
			}
			ans++;
		}
	}
}
