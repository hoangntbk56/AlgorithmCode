#include <bits/stdc++.h>

#define N 250052

using namespace std;

int n;
int perm[N], v[N];
pair<int, int> bit[N];

int parent[N];

struct Point {
	int x, y;
} a[N];

int m;

struct Edge {
	int u, v, len;

	bool operator < (const Edge &other) const {
		return len < other.len;
	}
} e[N * 4];

bool cmp(int i, int j) {
	return a[i].x < a[j].x || (a[i].x == a[j].x && a[i].y < a[j].y);
}

pair<int, int> query(int p) {
	int i = lower_bound(v, v + n, p) - v + 1;
	pair<int, int> ret(-1e9, -1e9);
	for (; i <= n; i += i & (-i)) {
		ret = max(ret, bit[i]);
	}
	return ret;
}

void update(int p, pair<int, int> val) {
	int i = lower_bound(v, v + n, p) - v + 1;
	for (; i; i -= i & (-i)) {
		bit[i] = max(bit[i], val);
	}
}

void process(int i) {
	int p = a[i].x - a[i].y;
	pair<int, int> ret = query(p);
	update(p, {a[i].x + a[i].y, i});
	if (ret.second >= 0) {
		e[m++] = {i, ret.second, a[i].x + a[i].y - ret.first};
	}
}

int go_up(int i) {
	return perm[i] == i ? i : perm[i] = go_up(perm[i]); 
}

int main() {
	cin >> n;//scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		cin >> a[i].x >> a[i].y;//scanf("%d %d", &a[i].x, &a[i].y);
		perm[i] = i;
	}
	for (int k = 0; k < 4; k++) {
		for (int i = 0; i < n; i++) {
			v[i] = a[i].x - a[i].y;
			bit[i + 1] = {-1e9, -1e9};
		}
		sort(v, v + n);
		sort(perm, perm + n, cmp);

		for (int i = 0; i < n; i++) {
			process(perm[i]);
		}

		if (k & 1) {
			for (int i = 0; i < n; i++) {
				a[i].y = -a[i].y;
			}
		} else {
			for (int i = 0; i < n; i++) {
				swap(a[i].x, a[i].y);
			}
		}
	}

	sort(e, e + m);
	for (int i = 0; i < n; i++) {
		perm[i] = i;
		v[i] = 1;
	}

	unsigned long long ans = 0;
	for (int i = 0; i < m; i++) {
		// cout << e[i].u << " " << e[i].v << " " << e[i].len << "\n";
		int x = go_up(e[i].u);
		int y = go_up(e[i].v);
		if (x != y) {
			perm[x] = y;
			ans += (unsigned long long) v[x] * v[y] * (e[i].len / 2); 
			v[y] += v[x];
			v[x] = 0;
		}
	}

	cout << ans;
}