#include <bits/stdc++.h>
#include <ext/pb_ds/tree_policy.hpp>
#include <ext/pb_ds/assoc_container.hpp>

using namespace std;
using namespace __gnu_pbds;
 
typedef long long ll;
typedef long double ld;
typedef complex<ld> cd;

typedef pair<ll,ll> pl;
typedef pair<ld,ld> pd;

typedef vector<ll> vi;
typedef vector<ld> vd;
typedef vector<ll> vl;
typedef vector<pl> vpl;
typedef vector<pl> vpl;
typedef vector<cd> vcd;

template <class T> using Tree = tree<T, null_type, less<T>, rb_tree_tag,tree_order_statistics_node_update>;

#define FOR(i, a, b) for (ll i=a; i<(b); i++)
#define F0R(i, a) for (ll i=0; i<(a); i++)
#define FORd(i,a,b) for (ll i = (b)-1; i >= a; i--)
#define F0Rd(i,a) for (ll i = (a)-1; i >= 0; i--)

#define sz(x) (ll)(x).size()
#define mp make_pair
#define pb push_back
#define f first
#define s second
#define lb lower_bound
#define ub upper_bound
#define all(x) x.begin(), x.end()

const ll MOD = 1000000007;
const ll INF = 1e18;
const ll MX = 200001;

template<ll SZ> struct Dijkstra {
    ll N;
    ll dist[SZ];
    vpl adj[SZ];
    priority_queue<pl,vector<pl>,greater<pl>> q;
    
    Dijkstra() {
        FOR(i,1,MX) dist[i] = INF;
    }
    
    void addEdge(ll A, ll B, ll L) {
        adj[A].pb({B,L}), adj[B].pb({A,L});
    }
    
    void gen() {
	    FOR(i,1,N+1) q.push({dist[i],i});
    	while (q.size()) {
    		pl x = q.top(); q.pop();
    		if (dist[x.s] < x.f) continue;
    		for (pl y: adj[x.s]) if (x.f+y.s < dist[y.f]) {
    			dist[y.f] = x.f+y.s;
    			q.push({dist[y.f],y.f});
    		}
    	}
    }
};

Dijkstra<MX> D;

ll N,M,Q;
map<ll,ll> adj[MX];
ll ans = 0;
vpl edge;
map<pl,vpl> m;

void tri(pl a, pl b) {
    // a.s+x-a.f 
    // b.s+b.f-x 
    // x = (b.s+b.f+a.f-a.s)/2
    ans = max(ans,b.s+b.f-a.f+a.s);
}

void solve(ll L, vpl cur) {
    map<ll,ll> M; // ll vs ll
    sort(all(cur));
    for (auto a: cur) {
        auto it = M.lb(a.s);
        if (it != M.end() && it->f-a.s+it->s <= a.f) continue;
        if (it != M.begin() && prev(it)->s+a.s-prev(it)->f <= a.f) continue;
        M[a.s] = a.f;
    }
    for (auto it = M.begin(); it != M.end(); it = next(it)) {
        if (it == M.begin()) ans = max(ans,2*(it->s+it->f));
        else tri(*prev(it),*it);
        
        if (next(it) == M.end()) ans = max(ans,2*(it->s+L-it->f));
        else tri(*it,*next(it));
    }
}

int main() {
    ios_base::sync_with_stdio(0); cin.tie(0);
    cin >> N >> M >> Q; assert( N <= 100000);
    assert(Q <= 100000);
    assert(M <= 200000);
    F0R(i,M) {
        // ll A = i+1, B = i+2, L = 1000000000;
        ll A,B,L; cin >> A >> B >> L; assert(A != B);
        adj[A][B] = adj[B][A] = L;
        D.addEdge(A,B,L);
        edge.pb({min(A,B),max(A,B)});
    }
    F0R(i,Q) {
        ll T,A,B,X; cin >> T >> A >> B >> X;
        if (A > B) {
            m[{B,A}].pb({T,adj[A][B]-X});
        } else {
            m[{A,B}].pb({T,X});
        }
        D.dist[A] = min(D.dist[A],T+X);
        D.dist[B] = min(D.dist[B],T+adj[A][B]-X);
    }
    D.N = N;
    D.gen();
    
    for (auto a: edge) {
        ll L = adj[a.f][a.s];
        vpl cur;
        for (auto x: m[a]) cur.pb(x);
        cur.pb({D.dist[a.f],0});
        cur.pb({D.dist[a.s],L});
        solve(L,cur);
    }
    cout << fixed << setprecision(1) << (ld)ans/2;
}

// read the question correctly (is y a vowel? what are the exact constralls?)
// look out for SPECIAL CASES (n=1?) and overflow (ll vs ll?) ARRAY OUT OF BOUNDSS