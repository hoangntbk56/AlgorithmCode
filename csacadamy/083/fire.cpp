//https://csacademy.com/contest/round-83/task/firestarter/

#include <bits/stdc++.h>

#define N 100005
#define M 200005

using namespace std;

int n, m, q;
map<pair<int, int>, int> mEdgeToIdx;

long long startTime[N + M];
vector<pair<int, int> > edges[N + M];

struct Edge {
	int u, v, w;
} e[M];

struct Query {
	int t, len;	

	bool operator < (const Query &other) const {
		return len < other.len;
	}
} query[N];

vector<Query> group[M];
priority_queue<pair<long long, int>, vector<pair<long long, int> >, greater<pair<long long, int> > > pq;

void addEdge(int u, int v, int w) {
	edges[u].push_back({v, w});
	edges[v].push_back({u, w});
}

void buildGraph() {
	for (int i = 1; i <= n + m; i++) {
		startTime[i] = 1LL << 60;
	}

	int k = n + 1;
	for (int i = 0; i < m; i++) {
		sort(group[i].begin(), group[i].end());
		if (group[i].size() > 0) {
			addEdge(e[i].u, k, group[i].front().len);
			addEdge(e[i].v, k + group[i].size() - 1, e[i].w - group[i].back().len);
			for (int j = 0; j < group[i].size() - 1; j++) {
				addEdge(k + j, k + j + 1, group[i][j + 1].len - group[i][j].len);
			}
			for (int j = 0; j < group[i].size(); j++) {
				startTime[k + j] = group[i][j].t;
				pq.push({startTime[k + j], k + j});
			}
			k += group[i].size();
		} else {
			addEdge(e[i].u, e[i].v, e[i].w);
		}
	}
	// for (int i = 0; i < m; i++) {
	// 	cout << e[i].u << " " << e[i].v << " " << e[i].w << "asdasdad\n";
	// }
	// for (int i = 1; i < k; i++) {
	// 	cout << edges[i].size() << "\n";
	// 	for (int j = 0; j < edges[i].size(); j++) {
	// 		cout << i << " " << edges[i][j].first << " " << edges[i][j].second << "\n";
	// 	}
	// }
}

int main() {
	scanf("%d %d %d", &n, &m, &q);
	for (int i = 0; i < m; i++) {
		scanf("%d %d %d", &e[i].u, &e[i].v, &e[i].w);
		if (e[i].u > e[i].v) {
			swap(e[i].u, e[i].v);
		}
		mEdgeToIdx[{e[i].u, e[i].v}] = i;
	}
	for (int i = 0; i < q; i++) {
		int t, u, v, len;
		scanf("%d %d %d %d", &t, &u, &v, &len);
		
		int idx;
		if (u > v) {
			swap(u, v);
			idx = mEdgeToIdx[{u, v}];
			len = e[idx].w - len;
		} else {
			idx = mEdgeToIdx[{u, v}];
		}
		group[idx].push_back({t, len});
	}
	buildGraph();
	// exit(0);
	long long res = 0;
	while (!pq.empty()) {
		int u = pq.top().second;
		long long cur = pq.top().first;
		pq.pop();
		if (startTime[u] == cur) {
			// cout << cur << " " << u << " --\n";
			res = max(res, cur * 2);
			for (auto &edge : edges[u]) {
				int v = edge.first;
				int len = edge.second;
				if (startTime[v] >= cur) {
					if (startTime[v] - cur <= len) {
						// cout << u << " " << v << " " << startTime[v] << " " << len << " " << cur << " asda\n";
						res = max(res, startTime[v] + len + cur);
					} else {
						startTime[v] = cur + len;
						pq.push({startTime[v], v});
					}
				} else {
					res = max(res, startTime[v] + len + cur);		
				}
			} 
		}
	}
	printf("%lld.%d", res >> 1, ((res & 1) ? 5 : 0));
}