#include <bits/stdc++.h>

#define N 100005
#define inf 1000000000

using namespace std;

int n;
int m;
int a[N], b[N];
pair<int, int> minmaxTree[N << 2];

pair<int, int> val[N];
pair<int, int> segments[N];
pair<int, int> debug[N];

int idx;
int scc[N];
int num[N];
int nxt[N];
int mark[N];

stack<int> st;
vector<int> order;
vector<int> group[N];

int tarjanTree[N << 2];

pair<int, int> merge(pair<int, int> x, pair<int, int> y) {
	return {min(x.first, y.first), max(x.second, y.second)};
}

void buildMinMaxTree(int l, int r, int i) {
	if (l == r) {
		minmaxTree[i] = {b[l], b[l]};
	} else {
		int mid = (l + r) >> 1;
		buildMinMaxTree(l, mid, i + i);
		buildMinMaxTree(mid + 1, r, i + i + 1);
		minmaxTree[i] = merge(minmaxTree[i + i], minmaxTree[i + i + 1]);
	}
}

void buildFinalTree(int l, int r, int i) {
	if (l == r) {
		minmaxTree[i] = val[l];
	} else {
		int mid = (l + r) >> 1;
		buildFinalTree(l, mid, i + i);
		buildFinalTree(mid + 1, r, i + i + 1);
		minmaxTree[i] = merge(minmaxTree[i + i], minmaxTree[i + i + 1]);	
	}
}

pair<int, int> getMinMax(int l, int r, int x, int y, int i) {
	if (l > y || r < x) {
		return {inf, -inf};
	}
	if (x <= l && r <= y) {
		return minmaxTree[i];
	} else {
		int mid = (l + r) >> 1;
		return merge(getMinMax(l, mid, x, y, i + i), getMinMax(mid + 1, r, x, y, i + i + 1));
	}
}

void updateMinMax(int l, int r, int u, pair<int, int> p, int i) {
	if (l == r) {
		minmaxTree[i] = p;
	} else {
		int mid = (l + r) >> 1;
		if (mid >= u) {
			updateMinMax(l, mid, u, p, i + i);
		} else {
			updateMinMax(mid + 1, r, u, p, i + i + 1);
		}
		minmaxTree[i] = merge(minmaxTree[i + i], minmaxTree[i + i + 1]);
	}
}

void updateNode(int l, int r, int u, int p, int i) {
	if (l == r) {
		tarjanTree[i] = p;
	} else {
		int mid = (l + r) >> 1;
		if (mid >= u) {
			updateNode(l, mid, u, p, i + i);
		} else {
			updateNode(mid + 1, r, u, p, i + i + 1);
		}
		tarjanTree[i] = min(tarjanTree[i + i], tarjanTree[i + i + 1]);
	}
}

	int getTarjanValue(int l, int r, int x, int y, int i) {
	if (l > y || r < x) {
		return inf;
	}
	if (x <= l && r <= y) {
		return tarjanTree[i];
	} else {
		int mid = (l + r) >> 1;
		return min(getTarjanValue(l, mid, x, y, i + i), getTarjanValue(mid + 1, r, x, y, i + i + 1));
	}
}

int findUnvisitedNode(int u) {
	return nxt[u] == 0 ? u : nxt[u] = findUnvisitedNode(nxt[u]);
}

int markVisitedNode(int u) {
	return nxt[u] = findUnvisitedNode(u + 1);
}

void dfs(int u) {
	st.push(u);
	scc[u] = num[u] = ++idx;
	scc[u] = min(scc[u], getTarjanValue(1, n - 1, segments[u].first, segments[u].second - 1, 1));
	updateNode(1, n - 1, u, num[u], 1);

	int v = findUnvisitedNode(segments[u].first);
	while (v < segments[u].second) {
		if (mark[v] == 0) {
			if (num[v] == 0) {
				dfs(v);
				scc[u] = min(scc[u], scc[v]);
			} 
		}
		v = markVisitedNode(v);
	}

	if (num[u] == scc[u]) {
		val[u] = segments[u];
		while (st.top() != u) {
			int v = st.top();
			updateNode(1, n - 1, v, inf, 1);
			val[u] = merge(val[u], segments[v]);
			group[u].push_back(v);
			mark[v] = 1;
			st.pop();
		}
		updateNode(1, n - 1, u, inf, 1);
		order.push_back(u);
		mark[u] = 1;
		st.pop();
	}
}

int main() {
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%d", a + i);
		b[a[i]] = i;
	}
	
	buildMinMaxTree(1, n, 1);
	for (int i = 1; i < n; i++) {
		int l = a[i];
		int r = a[i + 1];
		if (l > r) {
			swap(l, r);
		}
		segments[i] = getMinMax(1, n, l, r, 1);
		debug[i] = segments[i];
	}

	for (int i = 1; i <= n << 2; i++) {
		tarjanTree[i] = inf;
	}
	for (int i = 1; i < n; i++) {
		if (mark[i] == 0) {
			dfs(i);
		}
	}

	for (int i = 1; i <= n << 2; i++) {
		minmaxTree[i] = {inf, -inf};
	}

	for (int i = 0; i < order.size(); i++) {
		updateMinMax(1, n - 1, order[i], val[order[i]], 1);
		val[order[i]] = getMinMax(1, n - 1, val[order[i]].first, val[order[i]].second - 1, 1);
		for (int v : group[order[i]]) {
			val[v] = val[order[i]];
			updateMinMax(1, n - 1, v, val[v], 1);
		}
	}

	scanf("%d", &m);
	buildFinalTree(1, n - 1, 1);
	for (int i = 0; i < m; i++) {
		int l, r;
		scanf("%d %d", &l, &r);
		if (l == r) {
			printf("%d %d\n", l, l);
		} else {
			r--;
			pair<int, int> res = getMinMax(1, n - 1, l, r, 1);
			printf("%d %d\n", res.first, res.second);
		}
	}
}

/*
10
2 1 4 3 5 6 7 10 8 9
5
2 3
3 7
4 7
4 8
7 8
*/