#include <bits/stdc++.h>

#define N 300005

using namespace std;

int n, m;
pair<int, int> buffalos[N];
pair<int, int> settlers[N];

enum Type {
	BUFFALO,
	SETTLER
};

struct Event {
	int x;
	int y;
	Type type;
	int id;

	bool operator < (const Event &other) const {
		return y > other.y || (y == other.y && type > other.type);
	}
} events[N + N];

int parent[N], group[N];
int ans[N], num[N];

int find(int i) {
	return group[i] < 0 ? i : group[i] = find(group[i]);
}

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d %d", &buffalos[i].first, &buffalos[i].second);
		events[i] = {buffalos[i].first, buffalos[i].second, BUFFALO, i};
	}
	scanf("%d", &m);
	for (int i = 0; i < m; i++) {
		scanf("%d %d", &settlers[i].first, &settlers[i].second);
		events[i + n] = {settlers[i].first, settlers[i].second, SETTLER, i};	
	}

	sort(events, events + n + m);
	set<pair<int, int>, greater<pair<int, int>>> points;  
	for (int i = 0; i < n + m; i++) {
		Event e = events[i];
		if (e.type == SETTLER) {
			auto it = points.insert({e.x, e.id}).first;
			auto next = it;
			next++;
			while (next != points.end() && next->second > e.id) {
				points.erase(next);
				next = it;
				next++;
			}
			if (it != points.begin()) {
				it--;
				parent[e.id] = it->second;
			} else {
				parent[e.id] = -1;
			}
		} else {
			if (!points.empty()) {
				auto it = points.upper_bound({e.x, 0});
				if (it != points.begin()) {
					it--;
					num[it->second]++;
				}
			}
		}
	}

	for (int i = 0; i < m; i++) {
		group[i] = -1;
	}
	for (int i = m - 1; i >= 0; i--) {
		ans[i] = num[i];
		if (parent[i] >= 0) {
			int j = find(parent[i]);
			num[j] += num[i];
			group[i] = j;
		}
	}
	for (int i = 0; i < m; i++) {
		printf("%d\n", ans[i]);
	}
}