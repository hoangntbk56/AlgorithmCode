#include <bits/stdc++.h>

#define N 1000005

using namespace std;

int n, k;
int a[N];
int start[30];
int cnt[30];

int main() {
	scanf("%d %d", &n, &k);
	for (int i = 1; i <= n; i++) {
		scanf("%d", a + i);
		for (int b = 0; b < 30; b++) {
			if (((a[i] >> b) & 1)) {
				if (start[b] == 0) {
					start[b] = i;
				}
				cnt[b]++;
			}
		}
	}

	int mb;
	int pos = 0;
	for (int i = 29; i >= 0; i--) {
		if (cnt[i] >= k) {
			pos = start[i];
			mb = i;
			break;
		}
	}
	if (pos == 0) {
		printf("0");
		return 0;
	} 

	int res = 1 << mb;
	rotate(a + 1, a + pos, a + n + 1);
	int orl[30];
	int orr[30];
	int pl[30];
	int pr[30];

	a[n + 1] = a[1];
	for (int b = mb - 1; b >= 0; b--) {
		if (cnt[b] >= k) {
			int temp = res | (1 << b);

			int num = 0;
			int mask = 0;
			
			for (int i = 0; i < 30; i++) {
				orl[i] = orr[i] = -1;
			}
			orl[0] = orr[0] = a[1] & temp;
			pl[0] = n + 1;
			pr[0] = 1;
			mask = a[1] & temp;
			for (int i = 2, j = 1; i <= n; i++) {
				if ((mask | (a[i] & temp)) > mask) {
					mask |= (a[i] & temp);
					orr[j] = mask;
					pr[j++] = i;
				}
			}
			mask = a[1] & temp;
			for (int i = n, j = 1; i > 1; i--) {
				if ((mask | (a[i] & temp)) > mask) {
					mask |= (a[i] & temp);
					orl[j] = mask;
					pl[j++] = i;
				}
			}	
			int ok = 0;
			for (int i = 0; i < 30 && ok == 0; i++) {
				if (orl[i] < 0) {
					break;
				}
				for (int j = 0; j < 30; j++) {
					if (orr[j] < 0) {
						break;
					}
					if ((orl[i] | orr[j]) == temp) {
						num = 1;
						mask = 0;
						for (int x = pr[j] + 1; x < pl[i]; x++) {
							mask |= (a[x] & temp);
							if (mask == temp) {
								num++;
								mask = 0;
							}
						}
						if (num >= k) {
							ok = 1;
						}
						break;
					}
				}
			}
			if (ok) {
				res = temp;
			}
		}
	}
	printf("%d", res);
}