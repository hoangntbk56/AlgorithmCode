#include <bits/stdc++.h>

#define N 100010
#define MOD 1000000007

using namespace std;

int n;
int a[N], p[N], f[N];
int max_min[N << 2], min_max[N << 2], max_max[N << 2], min_min[N << 2];
int sum_max[N << 2], sum_min[N << 2], total[N << 2];

void apply_max(int i, int l, int r, int val) {
	if (max_max[i] < val) {
		max_min[i] = max_max[i] = val;
		sum_max[i] = 1LL * (f[r] - f[l - 1] + MOD) * val % MOD;
		total[i] = 1LL * sum_min[i] * val % MOD;
	}
}

void apply_min(int i, int l, int r, int val) {
	if (min_min[i] > val) {
		min_min[i] = min_max[i] = val;
		sum_min[i] = 1LL * (f[r] - f[l - 1] + MOD) * val % MOD;
		total[i] = 1LL * sum_max[i] * val % MOD;
	}
}

void upd_max(int l, int r, int x, int i, int val) {
	if (max_min[i] >= val) {
		return;
	}
	if (r <= x && max_max[i] < val) {
		apply_max(i, l, r, val);
	} else {
		int m = (l + r) >> 1;
		if (max_max[i] == max_min[i]) {
			apply_max(i + i, l, m, max_max[i]);
			apply_max(i + i + 1, m + 1, r, max_max[i]);
		} 
		if (min_min[i] == min_max[i]) {
			apply_min(i + i, l, m, min_min[i]);
			apply_min(i + i + 1, m + 1, r, min_min[i]);
		}
		upd_max(l, m, x, i + i, val);
		if (m < x) {
			upd_max(m + 1, r, x, i + i + 1, val);
		}
		max_max[i] = max_max[i + i];
		max_min[i] = max_min[i + i + 1];
		sum_max[i] = (sum_max[i + i] + sum_max[i + i + 1]) % MOD;
		total[i] = (total[i + i] + total[i + i + 1]) % MOD;
	}
}

void upd_min(int l, int r, int x, int i, int val) {
	if (min_max[i] <= val) {
		return;
	}
	if (r <= x && min_min[i] > val) {
		apply_min(i, l, r, val);
	} else {
		int m = (l + r) >> 1;
		if (max_max[i] == max_min[i]) {
			apply_max(i + i, l, m, max_max[i]);
			apply_max(i + i + 1, m + 1, r, max_max[i]);
		} 
		if (min_min[i] == min_max[i]) {
			apply_min(i + i, l, m, min_min[i]);
			apply_min(i + i + 1, m + 1, r, min_min[i]);
		}
		upd_min(l, m, x, i + i, val);
		if (m < x) {
			upd_min(m + 1, r, x, i + i + 1, val);
		}
		min_min[i] = min_min[i + i];
		min_max[i] = min_max[i + i + 1];
		sum_min[i] = (sum_min[i + i] + sum_min[i + i + 1]) % MOD;
		total[i] = (total[i + i] + total[i + i + 1]) % MOD;
	}

}

int main() {
	srand(time(0));
	n = 10000;//scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		a[i] = rand() % 1000000;//scanf("%d", a + i);
		//cout << a[i] << " ";
	}
	cout << "\n";
	p[0] = p[1] = 1;
	for (int i = 1; i < n; i++) {
		p[i + 1] = 1LL * p[i] * 2 % MOD; 
	}
	for (int i = 1; i <= n; i++) {
		f[i] = (f[i - 1] + p[i - 1]) % MOD;
	}
	for (int i = 1; i <= n << 2; i++) {
		max_min[i] = max_max[i] = 0;
		min_max[i] = min_min[i] = 1e9 + 1;
	}

	int ans = 0;
	for (int i = 1; i <= n; i++) {
		upd_max(1, n, i, 1, a[i]);
		upd_min(1, n, i, 1, a[i]);
		ans = (ans + 1LL * total[1] * p[n - i] % MOD) % MOD;
		//cout << ans << " " << total[1] << " " << p[n - i] << "\n"; 
	}

	int res = 0;
	for (int i = 1; i <= n; i++) {
		int mx = a[i];
		int mn = a[i];
		for (int j = i; j <= n; j++) {
			mx = max(mx, a[j]);
			mn = min(mn, a[j]);
			res = (res + 1LL * mx * mn % MOD * p[i - 1] % MOD * p[n - j] % MOD) % MOD;
		}	
	}

	printf("%d\n", ans);
	printf("%d\n", res);
}