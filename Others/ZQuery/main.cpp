// http://www.spoj.com/problems/ZQUERY/

#include <bits/stdc++.h>

using namespace std;

const int N = 50010;
const int B = 250;

int n, m;
int a[N], ans[N];
vector<pair<int, int> > queries[N];

int mx[B];
int last[N + N];
int pos[B][N + N];

int main() {
	a[1] = N;
	scanf("%d %d", &n, &m);
	for (int i = 2; i <= n + 1; i++) {
		scanf("%d", a + i);
		a[i] += a[i - 1];
	}
	for (int i = 0; i < m; i++) {
		int l, r;
		scanf("%d %d", &l, &r);
		queries[r + 1].emplace_back(l, i);
	}

	for (int i = 1; i <= n + 1; i++) {
		int b = i / B;
		for (int j = b; j >= 0; j--) {
			if (pos[j][a[i]]) {
				mx[j] = max(mx[j], i - pos[j][a[i]]);
			}
		}
		if (!pos[b][a[i]]) {
			pos[b][a[i]] = i;
		}
		last[a[i]] = i;

		for (auto &q : queries[i]) {
			int id = q.second;
			int x = q.first / B;
			for (int j = b; j > x; j--) {
				ans[id] = max(ans[id], mx[j]);
			}
			for (int j = q.first; j < min(i, (x + 1) * B); j++) {
				ans[id] = max(ans[id], last[a[j]] - j);
			}
		}
	}

	for (int i = 0; i < m; i++) {
		printf("%d\n", ans[i]);
	}
}