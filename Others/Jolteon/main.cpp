// https://csacademy.com/contest/romanian-ioi-2017-selection-6/task/jolteon/

#include <bits/stdc++.h>

using namespace std;

const int M = 1000010;
const int N = 100010;
const int B = 350;

int n;
int a[N], last[M], pre[M];
long long h[M], mask[N], mark[N], ans, res;
long long cnt[B + B];

int main() {
	clock_t begin = clock();
	
	scanf("%d", &n);
	for (int i = 1, idx = 0; i <= n; i++) {
		scanf("%d", a + i);
		if (h[a[i]] == 0) {
			h[a[i]] = ((1LL * rand()) << 32) | rand();
		}
		pre[i] = last[a[i]];
		last[a[i]] = i;

		if (i == n || i % B == 0) {			
			int l = idx * B + 1;
			for (int j = l; j <= i; j++) {
				for (int k = l; k <= pre[j]; k++) {
					mask[k] ^= h[a[j]];
				}
				for (int k = l; k <= j; k++) {
					ans += mask[k] == 0;
				}
			}

			vector<int> seg;
			seg.push_back(0);
			seg.push_back(l - 1);
			for (int j = l; j <= i; j++) {
				seg.push_back(pre[j]);
			}
			sort(seg.begin(), seg.end());
			seg.resize(unique(seg.begin(), seg.end()) - seg.begin());

			for (int j = 1; j < seg.size() && seg[j] < l; j++) {
				int x = seg[j - 1] + 1;
				int y = seg[j];
				long long state = 0;
				for (int k = l; k <= i; k++) {
					if (pre[k] >= y) {
						state ^= h[a[k]];
					}
					cnt[k - l] = state;
				}
				sort(cnt, cnt + i - l + 1);
				int ans2 = ans;
				for (int k = x; k <= y; k++) {
					ans += upper_bound(cnt, cnt + i - l + 1, mask[k]) - cnt;
					ans -= lower_bound(cnt, cnt + i - l + 1, mask[k]) - cnt;
					mask[k] ^= state;
				}
			}
			idx++;
		}
	}

	printf("%lld\n", ans);

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << elapsed_secs;
}