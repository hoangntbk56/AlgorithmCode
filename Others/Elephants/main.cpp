#include <bits/stdc++.h>

#define N 150005
#define M (N / B + 10)
#define B 350

using namespace std;

int ans = 0;
int n, limit, m, k;
int a[N], x[N], cnt[M], tmp[B + B], buckets[M][B + B], cost[M][B + B], last[M][B + B];

void process(int b) {
  for (int i = 0, j = 0; i < cnt[b]; i++) {
    for (;j + 1 < cnt[b] && buckets[b][j + 1] - buckets[b][i] <= limit; j++);
    tmp[i] = j;
  }
  for (int i = cnt[b] - 1; i >= 0; i--) {
    if (tmp[i] == cnt[b] - 1) {
      last[b][i] = i;
      cost[b][i] = 1;
    } else {
      last[b][i] = last[b][tmp[i] + 1];
      cost[b][i] = cost[b][tmp[i] + 1] + 1;
    }
  }
}

void build() {
  for (int b = 0, pos = 0; b < k; b++, pos += B) {
    cnt[b] = min(n - pos, B);
    for (int i = 0; i < cnt[b]; i++) {
      buckets[b][i] = x[pos + i];
    }
    process(b);
  }
}

void rebuild() {
  for (int b = 0, i = 0; b < k; b++) {
    for (int j = 0; j < cnt[b]; j++) {
      x[i++] = buckets[b][j];
    }
  }
  build();
}

void solve() {
  ans = 0;
  int i = cnt[0] > 0 ? 0 : 1;
  int start = 0;
  while (i < k) {
    int j = i + 1;
    while (j < k && (cnt[j] == 0 || buckets[j][cnt[j] - 1] - buckets[i][last[i][start]] <= limit)) {
      j++;
    }
    ans += cost[i][start];
    if (j < k) {
      start = upper_bound(buckets[j], buckets[j] + cnt[j], limit + buckets[i][last[i][start]]) - buckets[j];
    }
    i = j;
  }
  printf("%d\n", ans);
}

void update(int id, int p) {
  for (int b = 0; b < k; b++) {
    if (cnt[b] > 0 && buckets[b][0] <= a[id] && buckets[b][cnt[b] - 1] >= a[id]) {
      for (int i = 0; i < cnt[b]; i++) {
        if (buckets[b][i] == a[id]) {
          for (int j = i; j < cnt[b] - 1; j++) {
            buckets[b][j] = buckets[b][j + 1];
          }
          break;
        }
      }
      cnt[b]--;
      process(b);
      break;
    }
  }

  for (int b = k - 1; b >= 0; b--) {
    if ((cnt[b] > 0 && buckets[b][0] <= p) || b == 0) {
      for (int i = 0; i <= cnt[b]; i++) {
        if (i == cnt[b] || buckets[b][i] > p) {
          for (int j = cnt[b]; j > i; j--) {
            buckets[b][j] = buckets[b][j - 1];
          }
          cnt[b]++;
          buckets[b][i] = p;
          process(b);
          break;
        }
      }
      break;
    }
  }
  a[id] = p;
  solve();
}

int main() {
  freopen("input.txt", "r", stdin);
  freopen("output.txt", "w", stdout);
  scanf("%d %d %d", &n, &limit, &m);
  for (int i = 0; i < n; i++) {
    scanf("%d", x + i);
    a[i] = x[i];
  }
  k = (n - 1) / B + 1;

  build();
  for (int i = 0; i < m; i++) {
    if (i > 0 && i % B == 0) {
      rebuild();
    }
    int id, p, expected;
    scanf("%d %d", &id, &p);
    update(id, p);
  }
}
