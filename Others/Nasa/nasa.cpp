//https://vietnam-north17.kattis.com/problems/vietnam-north17.nasa

#include <bits/stdc++.h>

#define N 100005

using namespace std;

int n, m, lower, upper;
pair<int, int> transmissions[N];
pair<int, int> segments[N];
pair<int, int> dp[N];

int solve() {
	dp[0] = {0, 0};
	for (int i = 1; i <= n; i++) {
		if (segments[i].first == 0) {
			dp[i] = {dp[i - 1].first + lower, dp[i - 1].second + upper};
		} else {
			int l = max(segments[i].second - upper, dp[i - 1].first);
			int r = min(dp[i - 1].second, segments[i].first - 1);
			dp[i].first = max(segments[i].second, l + lower);
			dp[i].second = r + upper; 
		}
		if (dp[i].first > n) {
			return i - 1;
		} 
	}
}

int main() {
	freopen("nasa.inp", "r", stdin);
	freopen("nasa.out", "w", stdout);
	cin >> n >> lower >> upper >> m;
	for (int i = 0; i < m; i++) {
		cin >> transmissions[i].first >> transmissions[i].second;
		if (segments[transmissions[i].second].first == 0) {
			segments[transmissions[i].second].first = transmissions[i].first;
		} 
		segments[transmissions[i].second].second = transmissions[i].first;
	}	
	int res = solve();
	memset(segments, 0, sizeof segments);
	for (int i = 0; i < m; i++) {
		transmissions[i].first = n - transmissions[i].first + 1;
		transmissions[i].second = res - transmissions[i].second + 1;
		if (segments[transmissions[i].second].first == 0) {
			segments[transmissions[i].second].second = transmissions[i].first;
		} 
		segments[transmissions[i].second].first = transmissions[i].first;
	}
	solve();
	cout << res << "\n";
	for (int i = res; i; i--) {
		int cnt = 0;
		while (n > dp[i - 1].first && cnt < upper) {
			cout << res - i + 1 << " ";
			cnt++;
			n--;
		}
	}
}