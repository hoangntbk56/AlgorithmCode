//http://hsin.hr/pripreme2016/zadaci/drugi/zadaci.pdf

#include <bits/stdc++.h>

#define N 100005
#define INF (int)1e9

using namespace std;

int n, m;
int a[N];
int mark[N];
vector<pair<int, int> > segs[N];

bool cmp(int i, int j) {
	return a[i] < a[j];
}

struct Edge {
	int u;
	int v;
	int w;

	Edge() : u(0), v(0), w(0) {}

	Edge(int u, int v, int w) : u(u), v(v), w(w) {}

	Edge(pair<int, int> a, pair<int, int> b) {
		u = a.second;
		v = b.second;
		w = a.first + b.first;
	}

	void minimize(Edge other) {
		if (u == 0 || (other.u > 0 && w > other.w)) {
			u = other.u;
			v = other.v;
			w = other.w;
		}
	}
};

struct TPoint {

	bool removed[N];
	int idx[N << 2];
	Edge bestEdge[N << 2];
	pair<int, int> bestNode[N << 2];
	vector<pair<int, int> > collections[N << 2];

	void build(int l, int r, int i, int a[]) {
		if (l == r) {
			collections[i].resize(1, {a[l], l});
		} else {
			int mid = (l + r) >> 1;
			build(l, mid, i + i, a);
			build(mid + 1, r, i + i + 1, a);
			collections[i].resize(r - l + 1);
			merge(collections[i + i].begin(), collections[i + i].end(), 
				collections[i + i + 1].begin(), collections[i + i + 1].end(), 
				collections[i].begin());
		}
	}

	void build(int a[]) {
		build(1, n, 1, a);
		for (int i = 1; i < 4 * n; i++) {
			bestEdge[i].w = INF;
		}
	}

	void minimize(int i) {
		if (i + i < (n << 2)) {
			bestEdge[i].minimize(bestEdge[i + i]);
			bestEdge[i].minimize(bestEdge[i + i + 1]);
		}
	}
	
	void update(int i) {
		bestEdge[i] = {0, 0, INF};
		if (bestNode[i].second != 0) {
			while (idx[i] < collections[i].size() && removed[collections[i][idx[i]].second]) {
				idx[i]++;
			}
			if (idx[i] < collections[i].size()) {
				bestEdge[i] = Edge(bestNode[i], collections[i][idx[i]]);
			} 
		}
		minimize(i);
		if (bestEdge[i].w == 0) {
			cout << i << " wtf\n";
			exit(-1);
		}
	}

	void remove(int l, int r, int x, int i) {
		if (l == r) {
			idx[i]++;
			bestEdge[i] = {0, 0, INF};
		} else {
			int mid = (l + r) >> 1;
			if (mid >= x) {
				remove(l, mid, x, i + i); 
			} else {
				remove(mid + 1, r, x, i + i + 1);
			}
			update(i);
		}
	}

	void remove(int x) {
		removed[x] = 1;
		remove(1, n, x, 1);
	}

	void add(int l, int r, int x, int y, int i, pair<int, int> s) {
		if (x <= l && r <= y) {
			if (bestNode[i].second == 0 || bestNode[i].first > s.first) {
				bestNode[i] = s;
				update(i);
			}
		} else {
			int mid = (l + r) >> 1;
			if (mid >= x) {
				add(l, mid, x, y, i + i, s);
			}
			if (mid < y) {
				add(mid + 1, r, x, y, i + i + 1, s);
			}
			update(i);
		}
	}

	void add(vector<pair<int, int> > seg, pair<int, int> v) {
		for (auto s : seg) {
			add(1, n, s.first, s.second, 1, v);
		}
	}

	Edge getBestEdge() {
		return bestEdge[1];
	}
} tp;

struct TSegment {

	bool removed[N];
	int idx[N << 2];
	Edge bestEdge[N << 2];
	pair<int, int> bestNode[N << 2];
	vector<pair<int, int> > collections[N << 2];

	void build(int l, int r, int x, int y, int i, pair<int, int> val) {
		if (x <= l && r <= y) {
			collections[i].push_back(val);
		} else {
			int mid = (l + r) >> 1;
			if (mid >= x) {
				build(l, mid, x, y, i + i, val);
			} 
			if (mid < y) {
				build(mid + 1, r, x, y, i + i + 1, val);
			}
		}
	}

	void build(int a[], vector<pair<int, int> > seg[]) {
		vector<int> p(n);
		for (int i = 1; i <= n; i++) {
			p[i - 1] = i;
		}
		sort(p.begin(), p.end(), cmp);
		for (auto x : p) {
			for (auto s : seg[x]) {
				build(1, n, s.first, s.second, 1, {a[x], x});
			}
		}
		for (int i = 1; i < 4 * n; i++) {
			bestEdge[i].w = INF;
		}
	}

	void minimize(int i) {
		if (i + i < (n << 2)) {
			bestEdge[i].minimize(bestEdge[i + i]);
			bestEdge[i].minimize(bestEdge[i + i + 1]);
		}
	}
	
	void update(int i) {
		bestEdge[i] = {0, 0, INF};
		if (bestNode[i].second != 0) {
			while (idx[i] < collections[i].size() && removed[collections[i][idx[i]].second]) {
				idx[i]++;
			}
			if (idx[i] < collections[i].size()) {
				bestEdge[i] = Edge(bestNode[i], collections[i][idx[i]]);
			} 
		}
		minimize(i);
		if (bestEdge[i].w == 0) {
			cout << i << " wtf\n";
			exit(-1);
		}
	}

	void remove(int l, int r, int x, int y, int i) {
		if (x <= l && r <= y) {
			update(i);
		} else {
			int mid = (l + r) >> 1;
			if (mid >= x) {
				remove(l, mid, x, y, i + i);
			}
			if (mid < y) {
				remove(mid + 1, r, x, y, i + i + 1);
			}
			update(i);
		}
	}

	void remove(int x, vector<pair<int, int> > seg) {
		removed[x] = 1;
		for (auto s : seg) {
			remove(1, n, s.first, s.second, 1);
		}
	}

	void add(int l, int r, int x, int i, pair<int, int> s) {
		if (l < r) {
			int mid = (l + r) >> 1;
			if (mid >= x) {
				add(l, mid, x, i + i, s);
			} else {
				add(mid + 1, r, x, i + i + 1, s);
			}
		}
		if (bestNode[i].second == 0 || bestNode[i].first > s.first) {
			bestNode[i] = s;
		}
		update(i);
	}

	void add(int x, pair<int, int> s) {
		add(1, n, x, 1, s);
	}

	Edge getBestEdge() {
		return bestEdge[1];
	}
} ts;

void join(int u) {
	mark[u] = 1;
	tp.remove(u);
	tp.add(segs[u], {a[u], u});
	ts.remove(u, segs[u]);
	ts.add(u, {a[u], u});
}

int main() {
	freopen("pristojba.inp", "r", stdin);
  freopen("pristojba.out", "w", stdout);
	scanf("%d %d", &n, &m);
	for (int i = 1; i <= n; i++) {
		scanf("%d", a + i);
	}
	for (int i = 0; i < m; i++) {
		int u, l, r;
		scanf("%d %d %d", &u, &l, &r);
		segs[u].push_back({l, r});
	}

	tp.build(a);
	ts.build(a, segs);

	join(1);
	long long ans = 0;
	for (int i = 1; i < n; i++) {
		Edge e1 = tp.getBestEdge();
		Edge e2 = ts.getBestEdge();
		Edge e = (e2.u == 0 || (e1.u > 0 && e1.w < e2.w)) ? e1 : e2;
		cout << i - 1 << " " << e.u - 1 << " " << e.v - 1 << " " << e.w << endl;
		// cout << e1.u << " " << e1.v << " " << e1.w << endl;
		// cout << e2.u << " " << e2.v << " " << e2.w << endl;

		assert(e.w <= 2000000);
		if (mark[e.u]) {
			// cout << e.v << "\n";
			join(e.v);
		} else {
			// cout << e.u << "\n";
			join(e.u);
		}
		ans += e.w;
	}
	printf("%lld", ans);
}