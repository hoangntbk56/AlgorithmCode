// Problem L World Final 2017

#include <bits/stdc++.h>

using namespace std;

const int N = 200010;

int n;
pair<int, int> a[N];

int ans[N], pos[N], v[N], bit[N];
vector<pair<int, pair<int, int> > > rows, cols, queries;


bool cmp(int i, int j) {
	return a[i] < a[j];
}

void addQueries(pair<int, pair<int, int> > p) {
	assert(p.second.first <= p.second.second);
	queries.emplace_back(make_pair(p.second.first, make_pair(p.first, 1)));
	queries.emplace_back(make_pair(p.second.second + 1, make_pair(p.first, -1)));
}

void addRect(int i, int j) {
	if (a[i].first == a[j].first) {
		rows.emplace_back(make_pair(a[i].first, make_pair(a[i].second, a[j].second)));
	} else if (a[i].second == a[j].second) {
		cols.emplace_back(make_pair(a[i].second, make_pair(a[i].first, a[j].first)));
		addQueries(cols.back());
	} else {
		rows.emplace_back(make_pair(a[i].first, make_pair(a[i].second, a[j].second)));
		rows.emplace_back(make_pair(a[j].first, make_pair(a[i].second, a[j].second)));
		if (a[j].first - a[i].first > 1) {
			cols.emplace_back(make_pair(a[i].second, make_pair(a[i].first + 1, a[j].first - 1)));
			addQueries(cols.back());
			cols.emplace_back(make_pair(a[j].second, make_pair(a[i].first + 1, a[j].first - 1)));
			addQueries(cols.back());
		}
	}
}

int overlap(vector<pair<int, pair<int, int> > > &vec) {
	sort(vec.begin(), vec.end());
	for (int i = 1; i < vec.size(); i++) {
		if (vec[i].first == vec[i - 1].first && vec[i].second.first <= vec[i - 1].second.second) {
			return 1;
		}
	}
	return 0;
}

void update(int i, int w) {
	for (i += 5; i < N; i += i & (-i)) {
		bit[i] += w;
	}
}

int get(int i) {
	int w = 0;
	for (i += 5; i; i -= i & (-i)) {
		w += bit[i];
	}
	return w;
}

int main() {
	scanf("%d", &n);
	for (int i = 0; i < n + n; i++) {
		scanf("%d %d", &a[i].first, &a[i].second);
		v[i] = a[i].second;
		pos[i] = i;
	}
	sort(pos, pos + n, cmp);
	sort(pos + n, pos + n + n, cmp);

	set<pair<int, int> > s;
	for (int i = n - 1, j = n + n - 1; i >= 0; i--) {
		while (j >= n && a[pos[i]].first <= a[pos[j]].first) {
			s.insert(make_pair(a[pos[j]].second, pos[j]));
			j--;
		}
		auto it = s.lower_bound(make_pair(a[pos[i]].second, 0));
		if (it != s.end()) {
			addRect(pos[i], it->second);
			ans[pos[i]] = it->second;
			s.erase(it);
		} else {
			printf("syntax error");
			return 0;
		}
	}

	if (overlap(rows) || overlap(cols)) {
		printf("syntax error");
		return 0;
	}

	sort(v, v + n + n);
	int m = unique(v, v + n + n) - v;

	sort(queries.begin(), queries.end());
	for (int i = 0, j = 0; i < rows.size(); i++) {
		while (j < queries.size() && queries[j].first <= rows[i].first) {
			update(lower_bound(v, v + m, queries[j].second.first) - v, queries[j].second.second);
			j++;
		}
		int l = lower_bound(v, v + m, rows[i].second.first) - v;
		int r = lower_bound(v, v + m, rows[i].second.second) - v;
		if (get(r) - get(l - 1) > 0) {
			printf("syntax error");
			return 0;
		}
	}

	for (int i = 0; i < n; i++) {
		printf("%d\n", ans[i] - n + 1);
	}

}