// Problem D Wolrd Final 2017	

#include <bits/stdc++.h>

using namespace std;

const int N = 500005;

int n, m;
pair<int, int> a[N], b[N], A[N], B[N];

long long ans = 0;

void solve(int la, int ra, int lb, int rb) {
	if (la > ra || lb > rb) {
		return;
	}
	
	int mb = -1;
	long long val = -1;
	int ma = (la + ra) >> 1;
	for (int i = lb; i <= rb; i++) {
		if (B[i].first >= A[ma].first && B[i].second >= A[ma].second) {
			long long cost = 1LL * (B[i].first - A[ma].first) * (B[i].second - A[ma].second);
			if (cost > val) {
				val = cost;
				mb = i;
			}
		}
	}

	ans = max(ans, val);

	if (mb < 0) {
		mb = lb;
		while (mb <= rb && A[ma].first > B[mb].first) {
			mb++;
		}
	}
	solve(la, ma - 1, lb, mb);
	solve(ma + 1, ra, mb, rb);
}

int main() {
	scanf("%d %d", &n, &m);
	for (int i = 0; i < n; i++) {
		scanf("%d %d", &a[i].first, &a[i].second);
	}
	for (int i = 0; i < m; i++) {
		scanf("%d %d", &b[i].first, &b[i].second);
	}
	
	sort(a, a + n);
	sort(b, b + m);
	int k = 0;
	for (int i = n - 1; i >= 0; i--) {
		while (k && a[i].second <= A[k - 1].second) {
			k--;
		}
		A[k++] = a[i];
	}
	n = k;

	k = 0;
	for (int i = 0; i < m; i++) {
		while (k && b[i].second >= B[k - 1].second) {
			k--;
		}
		B[k++] = b[i];
	}
	m = k;

	reverse(A, A + n);
	solve(0, n - 1, 0, m - 1);

	printf("%lld\n", ans);
}