#include <bits/stdc++.h>

#define N 100005
#define MOD 1000000007

using namespace std;

long long L, R;
vector<int> p[N];
int cnt[N], dp[2][64];

int main() {
	freopen("I.in", "r", stdin);
	freopen("output.txt", "w", stdout);
	int T;
	cin >> T;
	while (T--) {
		cin >> L >> R;
		for (int i = 0; i <= R - L; i++) {
			p[i].clear();
		}
		for (int i = 2; i <= sqrt(R); i++) {
			long long j = ((L - 1) / i + 1) * i;
			while (j <= R) {
				p[j - L].push_back(i);
				j += i;
			}
		}
		for (int i = 0; i <= R - L; i++) {
			cnt[i] = 0;
			long long x = i + L;
			for (int v : p[i]) {
				while (x % v == 0) {
					cnt[i]++;
					x /= v;
				}
			}
			if (x > 1) {
				cnt[i]++;
			}
		}
		memset(dp[0], 0, sizeof dp[0]);
		dp[0][0] = 1;
		for (int i = 0; i <= R - L; i++) {
			memset(dp[1], 0, sizeof dp[1]);
			for (int j = 0; j < 64; j++) {
				if (dp[0][j]) {
					dp[1][j ^ cnt[i]] += dp[0][j];
					if (dp[1][j ^ cnt[i]] >= MOD) {
						dp[1][j ^ cnt[i]] -= MOD;
					}
				}
			}
			for (int j = 0; j < 64; j++) {
				dp[0][j] += dp[1][j];
				if (dp[0][j] >= MOD) {
					dp[0][j] -= MOD;
				}
			}
		}
		//int ans = 0;
		//for (int i = 1; i < 64; i++) {
		//	ans += dp[0][i];
		//	if (ans >= MOD) {
		//		ans -= MOD;
		//	}
		//}
		cout << dp[0][0] << "\n";
	}
}