// https://www.hackerrank.com/contests/w34/challenges/path-statistics

#include <bits/stdc++.h>

#define N 50005
#define B 256

using namespace std;

struct RangeQuery {
  int f[B * B];
  int sum[B];

  void add(int p) {
    sum[p / B]++;
    f[p]++;
  }

  void remove(int p) {
    assert(f[p] > 0);
    sum[p / B]--;
    f[p]--;
  }

  pair<int, int> search(int k) {
    for (int i = B - 1; i >= 0; i--) {
      if (k <= sum[i]) {
        for (int j = (i + 1) * B - 1; j >= i * B; j--) {
          if (f[j] >= k) {
            return make_pair(j, k);
          } else {
            k -= f[j];
          }
        }
      } else {
        k -= sum[i];
      }
    }
    assert(0);
  }
} global, local[B];

struct Query {
  int l, r, k, p, idx;

  bool operator < (const Query & other) const {
    return (l / B != other.l / B) ? l / B < other.l / B : r < other.r;
  }
} query[N];

int n, m, q;
int a[N], num[N], cnt[N];
vector<int> edge[N], big;

int len;
int lca[N][20];
int path[N + N], depth[N], start[N], finish[N];

int mark[N], frequency[N];

int ans[N];

void dfs(int u, int p) {
  start[u] = len;
  path[len] = u;
  len++;

  lca[u][0] = p;
  for (int i = 1; i < 20; i++) {
    lca[u][i] = lca[lca[u][i - 1]][i - 1];
  }
  depth[u] = depth[p] + 1;

  for (int v : edge[u]) {
    if (v != p) {
      dfs(v, u);
    }
  }

  finish[u] = len;
  path[len] = u;
  len++;
}

int getLCA(int u, int v) {
  if (depth[u] > depth[v]) {
    swap(u, v);
  }
  int delta = depth[v] - depth[u];
  for (int i = 19; i >= 0; i--) {
    if ((delta >> i) & 1) {
      v = lca[v][i];
    }
  }
  if (v == u) {
    return u;
  }
  for (int i = 19; i >= 0; i--) {
    if (lca[u][i] != lca[v][i]) {
      u = lca[u][i];
      v = lca[v][i];
    }
  }
  return lca[u][0];
}

void process(int u) {
  if (mark[u]) {
    if (frequency[a[u]] < B) {
      local[frequency[a[u]]].remove(a[u]);
    }
    global.remove(frequency[a[u]]);
    frequency[a[u]]--;
    if (frequency[a[u]]) {
      if (frequency[a[u]] < B) {
        local[frequency[a[u]]].add(a[u]);
      }
      global.add(frequency[a[u]]);
    }
  } else {
    if (frequency[a[u]]) {
      if (frequency[a[u]] < B) {
        local[frequency[a[u]]].remove(a[u]);
      }
      global.remove(frequency[a[u]]);
    }
    frequency[a[u]]++;
    if (frequency[a[u]] < B) {
      local[frequency[a[u]]].add(a[u]);
    }
    global.add(frequency[a[u]]);
  }
  mark[u] ^= 1;
}

int main() {
  scanf("%d %d", &n, &q);
  for (int i = 1; i <= n; i++) {
    scanf("%d", a + i);
    num[i - 1] = a[i];
  }
  for (int i = 1; i < n; i++) {
    int u, v;
    scanf("%d %d", &u, &v);
    edge[u].push_back(v);
    edge[v].push_back(u);
  }

  dfs(1, 0);
  sort(num, num + n);
  m = unique(num, num + n) - num;
  for (int i = 1; i <= n; i++) {
    a[i] = lower_bound(num, num + m, a[i]) - num;
    cnt[a[i]]++;
  }
  for (int i = m - 1; i >= 0; i--) {
    if (cnt[i] >= B) {
      big.push_back(i);
    }
  }

  for (int i = 0; i < q; i++) {
    int u, v, k;
    scanf("%d %d %d", &u, &v, &k);
    int p = getLCA(u, v);
    if (start[u] > start[v]) {
      swap(u, v);
    }
    if (p != u) {
      query[i] = {finish[u], start[v], k, p, i};
    } else {
      query[i] = {start[u], start[v], k, 0, i};
    }
  }
  sort(query, query + q);

  for (int i = 0, l = 0, r = -1; i < q; i++) {
    while (l < query[i].l) {
      process(path[l++]);
    }
    while (l > query[i].l) {
      process(path[--l]);
    }
    while (r < query[i].r) {
      process(path[++r]);
    }
    while (r > query[i].r) {
      process(path[r--]);
    }
    if (query[i].p) {
      process(query[i].p);
    }
    pair<int, int> ret = global.search(query[i].k);
    if (ret.first < B) {
      ans[query[i].idx] = local[ret.first].search(ret.second).first;
    } else {
      for (int j : big) {
        if (frequency[j] == ret.first) {
          ret.second--;
          if (ret.second == 0) {
            ans[query[i].idx] = j;
            break;
          }
        }
      }
    }
    if (query[i].p) {
      process(query[i].p);
    }
  }

  for (int i = 0; i < q; i++) {
    printf("%d\n", num[ans[i]]);
  }
}
