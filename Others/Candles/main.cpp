//https://csacademy.com/contest/round-41/task/candles/

#include <bits/stdc++.h>

#define N 100010

using namespace std;

int n, m;
int a[N];
int val[N << 2], lazy[N << 2], min_val[N << 2], max_val[N << 2];

void build(int l, int r, int i) {
	if (l == r) {
		min_val[i] = max_val[i] = a[l - 1];
	} else {
		int mid = (l + r) >> 1;
		build(l, mid, i + i);
		build(mid + 1, r, i + i + 1);
		min_val[i] = min(min_val[i + i], min_val[i + i + 1]);
		max_val[i] = max(max_val[i + i], max_val[i + i + 1]);
	}
}

void apply(int i, int v) {
	min_val[i] -= v;
	max_val[i] -= v;
	lazy[i] += v;
}

void lazy_propagate(int i) {
	if (lazy[i]) {
		apply(i + i, lazy[i]);
		apply(i + i + 1, lazy[i]);
		lazy[i] = 0;
	}
}

int get_val(int l, int r, int x, int i) {
	if (l == r) {
		return min_val[i];
	} 
	lazy_propagate(i);
	int mid = (l + r) >> 1;
	if (mid >= x) {
		return get_val(l, mid, x, i + i);
	} else {
		return get_val(mid + 1, r, x, i + i + 1);
	}
}

void decrease(int l, int r, int x, int y, int i) {
	if (x <= l && r <= y) {
		apply(i, 1);
	} else {
		lazy_propagate(i);
		int mid = (l + r) >> 1;
		if (mid >= x) {
			decrease(l, mid, x, y, i + i);
		}
		if (mid < y) {
			decrease(mid + 1, r, x, y, i + i + 1);
		}
		min_val[i] = min(min_val[i + i], min_val[i + i + 1]);
		max_val[i] = max(max_val[i + i], max_val[i + i + 1]);
	}
}

int lower(int l, int r, int val, int i) {
	if (max_val[i] < val) {
		return -1;
	}
	if (l == r) {
		return l;
	}
	lazy_propagate(i);
	int mid = (l + r) >> 1;
	int ret = lower(l, mid, val, i + i);
	if (ret < 0) {
		ret = lower(mid + 1, r, val, i + i + 1);
	}
	return ret;
}

int main() {
	scanf("%d %d", &n, &m);
	for (int i = 0; i < n; i++) {
		scanf("%d", a + i);
	}
	sort(a, a + n);
	build(1, n, 1);
	//cout << "aa\n";
	for (int i = 0; i < m; i++) {
		int x;
		scanf("%d", &x);
		int val = get_val(1, n, n - x + 1, 1);
		if (val == 0 || x > n) {
			printf("%d", i);
			return 0;
		}
		int l = lower(1, n, val, 1);
		int r = lower(1, n, val + 1, 1);
		//cout << i << " x = " << x << " val = " << val << " l = " << l << " r = " << r << "\n";
		if (r > 0) {
			decrease(1, n, r, n, 1);
			x -= n - r + 1;
		}
		decrease(1, n, l, l + x - 1, 1);
	}
	printf("%d", m);
}