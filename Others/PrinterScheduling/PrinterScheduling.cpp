// problem H, The 2016 ACM-ICPC Asia Nha Trang Regional Contest

#include <bits/stdc++.h>

#define N 205

using namespace std;

int n, m;
int idx[N];
pair<pair<int, int>, int> a[N];

int main() {
	freopen("H.in", "r", stdin);
	freopen("output.txt", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T--)	{
		scanf("%d %d", &n, &m);
		int maxT = 0;
		for (int i = 0; i < n; i++) {
			scanf("%d %d %d", &a[i].second, &a[i].first.first, &a[i].first.second);
			maxT = max(maxT, a[i].first.second);
		}
		sort(a, a + n);
		set<pair<int, int> > s;

		int stop = 0;
		for (int cur = 0, i = 0; cur <= maxT + 1; cur++) {
		
			for (auto &p : s) {
				if (a[p.second].first.second < cur) {
					stop = 1;
					break;
				}
			}
			if (stop) {
				printf("NO\n");
				break;
			}
			int j = 0;
			int k = min(m, (int) s.size());
			for (auto &p : s) {
				idx[j++] = p.second;
				if (j == k) {
					break;
				}
			}
			for (j = 0; j < k; j++) {
				s.erase(s.begin());
			}
			for (j = 0; j < k; j++) {
				a[idx[j]].second--;
				if (a[idx[j]].second > 0) {
					s.insert(make_pair(a[idx[j]].first.second - a[idx[j]].second, idx[j]));
				}
			}

			while (i < n && a[i].first.first == cur) {
				s.insert(make_pair(a[i].first.second - a[i].second, i));
				i++;
			}
		}
		if (!stop) {
			printf("YES\n");
		}
	}
}