// http://vn.spoj.com/problems/VQUERY/

#include <bits/stdc++.h>

#define N 100005

using namespace std;

struct Node {
	int val;
	int l, r;
	Node *left, *right;

	Node(int val, int l, int r) {
		this->l = l;
		this->r = r;
		this->val = val;
		this->left = this->right = 0;
	}

	Node(Node *left, Node *right) {
		this->l = left->l;
		this->r = right->r;
		this->left = left;
		this->right = right;
		this->val = max(left->val, right->val);
	}
} *moments[N];

int n, seq[N];
int R, Q;
int r[N], a[N], b[N], c[N], d[N];

Node* init(int l, int r) {
	if (l == r) {
		return new Node(seq[l], l, l);
	} else {
		int mid = (l + r) >> 1;
		return new Node(init(l, mid), init(mid + 1, r));
	}
}

Node* modify(Node* p, int x, int val) {
	if (p->l == p->r) {
		return new Node(val, p->l, p->r);
	} else {
		int mid = (p->l + p->r) >> 1;
		if (mid >= x) {
			return new Node(modify(p->left, x, val), p->right);
		} else {
			return new Node(p->left, modify(p->right, x, val));
		}
	}
}

int get(Node *p, int x, int y) {
	if (p->l > y || p->r < x) {
		return 0;
	}
	if (x <= p->l && p->r <= y) {
		return p->val;
	} else {
		return max(get(p->left, x, y), get(p->right, x, y));
	}
}

int main() {
	//freopen("input.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		scanf("%d", seq + i);
	}
	scanf("%d %d", &R, &Q);
	for (int i = 0; i < Q; i++) {
		scanf("%d %d %d %d %d", r + i, a + i, b + i, c + i, d + i);
	}

	moments[0] = init(1, n);

	long long L = 0;
	long long P = 0;
	for (int step = 0; step < R; step++) {
		for (int q = 0; q < Q; q++) {
			if (r[q] == 1) {
				int u = (L * a[q] + c[q]) % n + 1;
				int k = (L * b[q] + d[q]) % 1000000000 + 1;
				moments[P + 1] = modify(moments[P], u, k);
				P++;
			} else if (r[q] == 2) {
				int u = (L * a[q] + c[q]) % n + 1;
				int v = (L * b[q] + d[q]) % n + 1;
				if (u > v) {
					swap(u, v);
				}
				L = get(moments[P], u, v);
				printf("%d\n", (int)L);
			} else {
				int x = (L * a[q] + c[q]) % (P + 1);
				moments[P + 1] = moments[x];
				P++;
			}
		}
	}
}