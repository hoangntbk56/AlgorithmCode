#include <bits/stdc++.h>

#define N 300005
#define MAX_X 2e8

using namespace std;

int n, m, k, q;
int pos[N];

struct Store {
	int x, t, a, b;
} stores[N];

pair<int, int> t[N + N];
pair<int, pair<int, int> > queries[N];

set<pair<int, int> > occur[N];
set<pair<int, int> > orderedSegLeft[N];
set<pair<int, int> > orderedSegRight[N];
int segLeftTree[N * 4], segRightTree[N * 4];

int ans[N];

void initTree(int l, int r, int i) {
	segLeftTree[i] = 0;
	segRightTree[i] = 3 * MAX_X;
	if (l < r) {
		int mid = (l + r) >> 1;
		initTree(l, mid, i + i);
		initTree(mid + 1, r, i + i + 1);
	}
}

void addLeftTree(int l, int r, int p, int val, int i) {
	if (l == r) {
		segLeftTree[i] = val;
	} else {
		int mid = (l + r) >> 1;
		if (mid >= p) {
			addLeftTree(l, mid, p, val, i + i);
		} else {
			addLeftTree(mid + 1, r, p, val, i + i + 1);
		}
		segLeftTree[i] = max(segLeftTree[i + i], segLeftTree[i + i + 1]);
	}
}

void addRightTree(int l, int r, int p, int val, int i) {
	if (l == r) {
		segRightTree[i] = val;
	} else {
		int mid = (l + r) >> 1;
		if (mid >= p) {
			addRightTree(l, mid, p, val, i + i);
		} else {
			addRightTree(mid + 1, r, p, val, i + i + 1);
		}
		segRightTree[i] = min(segRightTree[i + i], segRightTree[i + i + 1]);
	}
}

int getLeftTree(int l, int r, int p, int i) {
	if (l == r) {
		return pos[l];
	} 
	int ret = -1;
	int mid = (l + r) >> 1;
	if (segLeftTree[i + i] >= p) {
		ret = getLeftTree(l, mid, p, i + i);
	} else {
		if (pos[mid + 1] <= p) {
			ret = getLeftTree(mid + 1, r, p, i + i + 1);
		}
	}
	return ret;
}

int getRightTree(int l, int r, int p, int i) {
	if (l == r) {
		return pos[l];
	}
	int ret = -1;
	int mid = (l + r) >> 1;
	if (segRightTree[i + i + 1] <= p) {
		ret = getRightTree(mid + 1, r, p, i + i + 1);
	} else {
		if (pos[mid] >= p) {
			ret = getRightTree(l, mid, p, i + i);
		}
	}
	return ret;
}

void addSegment(int l, int r, int t) {
	int mid = (l + r) >> 1;
	l = lower_bound(pos, pos + m, l) - pos;
	r = lower_bound(pos, pos + m, r) - pos;
	if (orderedSegLeft[l].empty() || orderedSegLeft[l].crbegin()->first < mid) {
		addLeftTree(0, m - 1, l, mid, 1);
	}
	orderedSegLeft[l].insert({mid, t});
	if (orderedSegRight[r].empty() || orderedSegRight[r].begin()->first > mid + 1) {
		addRightTree(0, m - 1, r, mid + 1, 1);
	}
	orderedSegRight[r].insert({mid + 1, t});
}

void removeSegment(int l, int r, int t) {
	int mid = (l + r) >> 1;
	l = lower_bound(pos, pos + m, l) - pos;
	r = lower_bound(pos, pos + m, r) - pos;
	orderedSegLeft[l].erase({mid, t});
	if (!orderedSegLeft[l].empty()) {
		if (orderedSegLeft[l].crbegin()->first < mid) {
			addLeftTree(0, m - 1, l, orderedSegLeft[l].crbegin()->first, 1);
		}
	} else {
		addLeftTree(0, m - 1, l, 0, 1);
	}
	orderedSegRight[r].erase({mid + 1, t});
	if (!orderedSegRight[r].empty()) {
		if (orderedSegRight[r].begin()->first > mid + 1) {
			addRightTree(0, m - 1, r, orderedSegRight[r].begin()->first, 1);
		}
	} else {
		addRightTree(0, m - 1, r, 3 * MAX_X, 1);
	}
}

int main() {
	scanf("%d %d %d", &n, &k, &q);
	for (int i = 0; i < n; i++) {
		scanf("%d %d %d %d", &stores[i].x, &stores[i].t, &stores[i].a, &stores[i].b);
		stores[i].x += MAX_X;
		t[i] = make_pair(stores[i].a, i);
		t[i + n] = make_pair(stores[i].b + 1, i);
		pos[i] = stores[i].x;
	}
	for (int i = 0; i < q; i++) {
		scanf("%d %d", &queries[i].second.first, &queries[i].first);
		queries[i].second.first += MAX_X;
		queries[i].second.second = i;
	}

	pos[n] = 0;
	pos[n + 1] = 3 * MAX_X;
	sort(pos, pos + n + 2);
	m = unique(pos, pos + n + 2) - pos;
	initTree(0, m - 1, 1);

	for (int i = 1; i <= k; i++) {
		occur[i].insert({0, 0});
		occur[i].insert({3 * MAX_X, 0});
		addSegment(0, 3 * MAX_X, i);
	}

	sort(t, t + n + n);
	sort(queries, queries + q);

	for (int i = 0, j = 0; i < q; i++) {
		while (j < n + n && t[j].first <= queries[i].first) {
			int vt = t[j].second;
			if (stores[vt].a == t[j].first) {
				// add 
				auto it = occur[stores[vt].t].lower_bound({stores[vt].x, 0});
				if (it->first == stores[vt].x) {
					if (stores[it->second].b < stores[vt].b) {
						occur[stores[vt].t].erase(it);
						occur[stores[vt].t].insert({stores[vt].x, vt});
					}
				} else {
					int r = it->first;
					it--;
					int l = it->first;
					occur[stores[vt].t].insert({stores[vt].x, vt});

					removeSegment(l, r, stores[vt].t);
					addSegment(l, stores[vt].x, stores[vt].t);
					addSegment(stores[vt].x, r, stores[vt].t);
				} 
			} else {
				// remove
				occur[stores[vt].t].erase({stores[vt].x, vt});
				auto it = occur[stores[vt].t].lower_bound({stores[vt].x, 0});
				if (it->first != stores[vt].x) {
					int r = it->first;
					it--;
					int l = it->first;
					removeSegment(l, stores[vt].x, stores[vt].t);
					removeSegment(stores[vt].x, r, stores[vt].t);
					addSegment(l, r, stores[vt].t);
				}
			}
			j++;
		}
		int retLeft = getLeftTree(0, m - 1, queries[i].second.first, 1);
		int retRight = getRightTree(0, m - 1, queries[i].second.first, 1);
		
		assert(retLeft != -1 || retRight != -1);

		int valueLeft = (retLeft >= 0) ? queries[i].second.first - retLeft : 0;
		int valueRight = (retRight >= 0) ? retRight - queries[i].second.first : 0;
		int value = max(valueLeft, valueRight);
		if (value >= 1e8) {
			value = -1;
		}
		ans[queries[i].second.second] = value;
	}

	for (int i = 0; i < q; i++) {
		printf("%d\n", ans[i]);
	}
}